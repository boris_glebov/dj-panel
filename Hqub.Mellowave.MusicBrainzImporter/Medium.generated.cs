#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Data.Common;
using System.Collections.Generic;
using Telerik.OpenAccess;
using Telerik.OpenAccess.Metadata;
using Telerik.OpenAccess.Data.Common;
using Telerik.OpenAccess.Metadata.Fluent;
using Telerik.OpenAccess.Metadata.Fluent.Advanced;
using Hqub.Mellowave.MusicBrainzImporter;


namespace Hqub.Mellowave.MusicBrainzImporter	
{
	public partial class Medium
	{
		private int _id;
		public virtual int Id 
		{ 
		    get
		    {
		        return this._id;
		    }
		    set
		    {
		        this._id = value;
		    }
		}
		
		private int _tracklist;
		public virtual int Tracklist 
		{ 
		    get
		    {
		        return this._tracklist;
		    }
		    set
		    {
		        this._tracklist = value;
		    }
		}
		
		private int _release;
		public virtual int Release 
		{ 
		    get
		    {
		        return this._release;
		    }
		    set
		    {
		        this._release = value;
		    }
		}
		
		private int _position;
		public virtual int Position 
		{ 
		    get
		    {
		        return this._position;
		    }
		    set
		    {
		        this._position = value;
		    }
		}
		
		private int? _format;
		public virtual int? Format 
		{ 
		    get
		    {
		        return this._format;
		    }
		    set
		    {
		        this._format = value;
		    }
		}
		
		private string _name;
		public virtual string Name 
		{ 
		    get
		    {
		        return this._name;
		    }
		    set
		    {
		        this._name = value;
		    }
		}
		
		private int _edits_pending;
		public virtual int Edits_pending 
		{ 
		    get
		    {
		        return this._edits_pending;
		    }
		    set
		    {
		        this._edits_pending = value;
		    }
		}
		
		private DateTime? _last_updated;
		public virtual DateTime? Last_updated 
		{ 
		    get
		    {
		        return this._last_updated;
		    }
		    set
		    {
		        this._last_updated = value;
		    }
		}
		
		private Tracklist _tracklist1;
		public virtual Tracklist TracklistRef 
		{ 
		    get
		    {
		        return this._tracklist1;
		    }
		    set
		    {
		        this._tracklist1 = value;
		    }
		}
		
		private Release _release1;
		public virtual Release ReleaseRef 
		{ 
		    get
		    {
		        return this._release1;
		    }
		    set
		    {
		        this._release1 = value;
		    }
		}
		
	}
}
