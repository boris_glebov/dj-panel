﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;

namespace Hqub.Mellowave.MusicBrainzImporter.Models
{
    /// <summary>
    /// В большей степени является представлением сущности Recording
    /// </summary>
    public class Track
    {
        public Track()
        {

        }

        [BsonElement("mb_pid")]
        public int MBPid { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("search_name")]
        public string SearchName { get; set; }

        /// <summary>
        /// Id сущности Track.
        /// </summary>
        [BsonElement("mb_track_pid")]
        public int MBTrackPid { get; set; }

        [BsonElement("mb_name")]
        public int MBName { get; set; }

        [BsonElement("duration")]
        public int Duration { get; set; }

        [BsonElement("mbid")]
        public string MBId { get; set; }

        [BsonElement("position")]
        public int Position { get; set; }

        [BsonElement("last_update")]
        public DateTime LastUpdate { get; set; }

        [BsonElement("mb_last_update")]
        public DateTime? MBLastUpdate { get; set; }

        [BsonElement("mb_track_last_update")]
        public DateTime? MBTrackLastUpdate { get; set; }

        [BsonElement("release")]
        public string Release { get; set; }
    }
}
