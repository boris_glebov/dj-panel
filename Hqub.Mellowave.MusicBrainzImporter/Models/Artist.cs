﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;

namespace Hqub.Mellowave.MusicBrainzImporter.Models
{
    [MongoRepository.CollectionName("Artists")]
    public class Artist : MongoRepository.Entity
    {
        public Artist()
        {
            Credits = new List<Credits>();
            Tracks = new List<Track>();
        }

        [BsonElement("mb_pid")]
        public int MBPid { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("mb_name")]
        public int MBName { get; set; }

        [BsonElement("bio")]
        public string Bio { get; set; }

        [BsonElement("begin_date_day")]
        public int? BeginDateDay { get; set; }

        [BsonElement("begin_date_month")]
        public int? BeginDateMonth { get; set; }

        [BsonElement("begin_date_year")]
        public int? BeginDateYear { get; set; }

        [BsonElement("end_date_day")]
        public int? EndDateDay { get; set; }

        [BsonElement("end_date_month")]
        public int? EndDateMonth { get; set; }

        [BsonElement("end_date_year")]
        public int? EndDateYear { get; set; }

        [BsonElement("mb_artist_type")]
        public int? MBArtistType { get; set; }

        [BsonElement("artist_type")]
        public string ArtistType { get; set; }

        [BsonElement("mb_country")]
        public int? MBCountry { get; set; }

        [BsonElement("country")]
        public string Country { get; set; }

        [BsonElement("mbid")]
        public string MBId { get; set; }

        [BsonElement("last_update")]
        public DateTime LastUpdate { get; set; }

        [BsonElement("mb_last_update")]
        public DateTime? MBLastUpdate { get; set; }

        [BsonElement("search_name")]
        public string SearchName { get; set; }

        [BsonElement("sort_name")]
        public string SortName { get; set; }

        [BsonElement("mb_sort_name")]
        public int MBSortName { get; set; }

        [BsonElement("credits")]
        public List<Credits> Credits { get; set; }

        [BsonElement("tracks")]
        public List<Track> Tracks { get; set; }

        [BsonElement("images")]
        public List<string> Images { get; set; }

        [BsonElement("track_count")]
        public int TrackCount { get; set; }
    }
}
