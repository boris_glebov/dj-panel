﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;

namespace Hqub.Mellowave.MusicBrainzImporter.Models
{
    [MongoRepository.CollectionName("Genres")]
    class Genre : MongoRepository.Entity
    {
        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("search_name")]
        public string SearchName { get; set; }
    }
}
