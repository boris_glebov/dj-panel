﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;

namespace Hqub.Mellowave.MusicBrainzImporter.Models
{
    public class Credits
    {
        [BsonElement("id")]
        public int Id { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("join_phrase")]
        public string JoinPhrase { get; set; }

        [BsonElement("position")]
        public int Position { get; set; }
    }
}
