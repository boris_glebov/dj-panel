﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZuneWebApi;

namespace Hqub.Mellowave.MusicBrainzImporter.Utilities
{
    /// <summary>
    /// Класс пытающийся найти фото артистов в разных источниках
    /// </summary>
    public static class ArtistImageSearchHelper
    {
        private static LastFmApi.LastFm _lastFmApi;
        private static Zune _zuneApi;

        static ArtistImageSearchHelper()
        {
            // Init Last and Zune API:
            _lastFmApi =
                new LastFmApi.LastFm(Properties.Settings.Default.LastfmAPIKey,
                                     Properties.Settings.Default.LastfmSecret);

            _zuneApi = new Zune();
        }

        public static ObservableCollection<string> GetAtistImages(string artist)
        {
            var images = new List<string>();

            var zuneArtist = _zuneApi.SearchArtist(artist).FirstOrDefault();

            if (zuneArtist != null)
            {
                var zuneImages = _zuneApi.GetArtistImages(zuneArtist.Id);

                if (zuneImages != null && zuneImages.Count > 0)
                    images.AddRange(zuneImages);
            }

            var lastImages = _lastFmApi.ArtistGetImages(artist, string.Empty, 20, true);

            if (lastImages != null && lastImages.Count > 0)
                images.AddRange(lastImages);

            return new ObservableCollection<string>(images);
        }



    }
}
