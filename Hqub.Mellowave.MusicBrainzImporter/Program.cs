﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver.Builders;

namespace Hqub.Mellowave.MusicBrainzImporter
{
    internal class Program
    {
        private static NLog.Logger _logger = NLog.LogManager.GetLogger("MAIN");
        private static int ArtistCount = 0;


        private static void Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += (sender, a) =>
                                                              {
                                                                  var exception = ((Exception) a.ExceptionObject);
                                                                  _logger.ErrorException(
                                                                      string.Format("{0}", exception.Message), exception);
                                                              };

            MainMenu();

            Console.WriteLine("END.");
            Console.ReadKey();
        }

        private static void MainMenu()
        {
            int code;
            do
            {
                Console.Clear();
                Console.WriteLine("1. Import all artist from MuzicBrainz db");
                Console.WriteLine("2. Import artist by Id from MusicBrainz db");
                Console.WriteLine("3. Generate search name for Artists");
                Console.WriteLine("4. Generate search name for Genre");
                Console.WriteLine("5. Calc tracks");
                Console.WriteLine("0. Exit.");

                Console.WriteLine();

                Console.Write("Choose action: ");

                var inputCode = Console.ReadLine();

                if (string.IsNullOrEmpty(inputCode))
                    continue;

                if (!int.TryParse(inputCode.Trim(), out code)) continue;

                InvokeAction(code);

                break;

            } while (true);
        }

        private static void InvokeAction(int code)
        {
            switch (code)
            {
                case 1:
                    StartImport();
                    break;

                case 2:
                    StartImportByArtistId();
                    break;
                case 3:
                    GenerateArtistSearchName();
                    break;
                case 4:
                    GenerateGenreSearchName();
                    break;
                case 5:
                    RecalcTrackCount();
                    break;
            }
        }

        private static void StartImport()
        {
            int MAX_ROW = 1000;

            var artistCount = 0;
            using (var context = new MusicBrainzModel())
            {
                artistCount = context.Artist_names.Count();
                Console.WriteLine("Artist {0} rows.", artistCount);
                _logger.Trace(string.Format("Artist {0} rows.", artistCount));
            }


            var counterI = Properties.Settings.Default.StartPage;
            while (counterI < artistCount)
            {
                using (var context = new MusicBrainzModel())
                {
                    var artists = context.Artist_names.Skip(MAX_ROW * counterI).Take(MAX_ROW).ToList();
                    if (!artists.Any())
                        break;

                    ++counterI;

                    ImportArtistsByArtistName(artists);

                    Console.WriteLine("Part {0}. Row: {1}", counterI, MAX_ROW);
                    _logger.Trace(string.Format("Part {0}. Row: {1}", counterI, MAX_ROW));
                }
            }

            Console.WriteLine("\nEND.");
        }

        private static void StartImportByArtistId()
        {
            int artistId = 0;

            Console.Write("Input artist id: ");

            if (!int.TryParse(Console.ReadLine(), out artistId))
            {
                WriteColor("Uncorrect format id.", ConsoleColor.Red);
                Console.ReadKey();

                return;
            }

            using (var context = new MusicBrainzModel())
            {
                var artists = context.Artists.Where(x => x.Id == artistId);
                if (!artists.Any())
                    return;

                ImportArtists(artists);

            }
        }

        private static void ImportArtistsByArtistName(IEnumerable<Artist_name> artistNames)
        {
            foreach (var artistName in artistNames)
            {
                WriteColor(string.Format("Execute Artist: {0}", artistName.Name), ConsoleColor.Yellow);
                ImportArtists(artistName.Artists);
            }
        }

        private static void ImportArtists(IEnumerable<Artist> artists)
        {
            // Init Last and Zune API:
//            var lastFmApi =
//                new LastFmApi.LastFm(Properties.Settings.Default.LastfmAPIKey,
//                                     Properties.Settings.Default.LastfmSecret);

            foreach (var artist in artists)
            {
                var artistRepository = new MongoRepository.MongoRepository<Models.Artist>();

                var artist1 = artist;
                if (artistRepository.Exists(x => x.MBPid == artist1.Id))
                {
                    continue;
                }

                var firstArtistCreditName = artist.ArtistCreditNamesRef.FirstOrDefault();
                if (firstArtistCreditName == null)
                {
                    _logger.Error(string.Format("Artist '{0}' creditName is null", artist.Name));
                    continue;
                }

                // Создаем запись Артиста

                var artistEntity = new Models.Artist
                {
                    Id = MongoDB.Bson.ObjectId.GenerateNewId().ToString(),
                    MBId = artist.Gid.ToString(),
                    MBPid = artist.Id,
                    MBName = artist.Name,
                    MBSortName = artist.Sort_name,
                    Name = firstArtistCreditName.ArtistNameRef.Name,
                    SortName = artist.ArtistSortNameRef.Name,
                    SearchName = ConvertListSearchWordToSearchName(GenerateSearchNameFromArtistMB(artist1)),
                    BeginDateDay = artist.Begin_date_day,
                    BeginDateMonth = artist.Begin_date_month,
                    BeginDateYear = artist.Begin_date_year,
                    EndDateDay = artist.End_date_day,
                    EndDateMonth = artist.End_date_month,
                    EndDateYear = artist.End_date_year,
                    MBArtistType = artist.Type,
                    ArtistType =
                        artist.ArtistTypeRef != null
                            ? artist.ArtistTypeRef.Name
                            : string.Empty,
                    MBCountry = artist.Country,
                    Country =
                        artist.Country != null
                            ? artist.CountryRef.Name
                            : string.Empty,
                    MBLastUpdate = artist.Last_updated,
                    LastUpdate = DateTime.Now
                };

                #region Получаем биографию артиста
//                try
//                {
//                    var bio = lastFmApi.ArtistGetBio(artistEntity.Name, artistEntity.MBId, true);
//                    artistEntity.Bio = ClearBio(bio);
//
//                    artistEntity.Images =
//                        new List<string>(
//                            Utilities.ArtistImageSearchHelper.GetAtistImages(artistEntity.Name));
//
//                    if (string.IsNullOrEmpty(bio))
//                    {
//                        var message = string.Format("{0} bio not found.", artistEntity.Name);
//                        _logger.Error(message);
//                        WriteColor(message, ConsoleColor.Red);
//                    }
//                    else
//                    {
//                        Console.WriteLine("\t{0}", bio);
//                    }
//                }
//                catch (Exception exception)
//                {
//                    _logger.ErrorException(string.Format("Lastfm error. {0}", exception.Message), exception);
//                }
                #endregion

                #region Добавляем трэки

                foreach (var artistCreditName in artist.ArtistCreditNamesRef)
                {
                    try
                    {
                        var release_name = string.Empty;
                        foreach (var recording in artistCreditName.ArtistCreditRef.Recordings)
                        {
                            var track = recording.Tracks.FirstOrDefault();
                            if (track == null)
                                continue;

                            var media = track.TracklistRef.Media.FirstOrDefault();
                            if (media == null)
                                continue;

                            // Получаем название альбома и трэка
                            // Пишем в лог и выводим на консоль
                            if (release_name == string.Empty)
                            {
                                release_name = media.ReleaseRef.ReleaseNameRef.Name;
                            }

                            var trackEntity = new Models.Track
                            {
                                MBPid = recording.Id,
                                MBId = recording.Gid.ToString(),
                                MBTrackPid = track.Id,
                                MBName = track.Name,
                                Name = track.TrackNameRef.Name,
                                SearchName = ClearName(track.TrackNameRef.Name),
                                Duration = recording.Length ?? 0,
                                Position = track.Position,
                                LastUpdate = DateTime.Now,
                                MBLastUpdate = recording.Last_updated,
                                MBTrackLastUpdate = track.Last_updated,
                                Release = release_name
                            };

                            // Добавляем исполнителя в коллекцию:
                            artistEntity.Tracks.Add(trackEntity);
                        }


                    }
                    catch (Exception exception)
                    {
                        var message = string.Format("\n\n{0}. {1}\n {2}\n\n", artist.Id,
                                                    artist.ArtistNameRef.Name,
                                                    exception.Message);

                        _logger.ErrorException(message, exception);

                        WriteColor(message, ConsoleColor.Red);
                    }
                }

                #endregion

                #region Сохраняем исполнителя в базу

                try
                {
                    artistRepository.Add(artistEntity);

                    Console.Clear();
                    WriteColor(string.Format("Success added {0} artists. Last {1}.", ++ArtistCount, artistEntity.Name),
                               ConsoleColor.Green);
                }
                catch (Exception exception)
                {
                    var message = string.Format("ERROR. Error save '{0}'\n{1}.",
                                                artistEntity.Name, exception.Message);
                    _logger.ErrorException(message, exception);

                    WriteColor(message, ConsoleColor.Red);
                }

                #endregion
            }
        }

        private static void GenerateArtistSearchName()
        {
            var artistRepository = new MongoRepository.MongoRepository<Models.Artist>();

            var counter = 0;
            foreach (var artist in artistRepository.Collection.FindAll())
            {
                try
                {
                    ++counter;
                    WriteColor(string.Format("{0}", artist.Name), ConsoleColor.Yellow);

                    var artistNameWords = artist.Name.ToLower().Split(' ');

                    var sb = new StringBuilder();

                    if (artistNameWords.Length > 1)
                    {
                        foreach (var artistNameWord in artistNameWords)
                        {
                            sb.AppendFormat("{0} ", ConvertListSearchWordToSearchName(GenerateSearchParts(artistNameWord)));
                        }
                    }

                    sb.Append(ConvertListSearchWordToSearchName(GenerateSearchParts(artist.Name.ToLower())));

                    artist.SearchName = sb.ToString();
                    artist.SortName = artist.Name.ToLower();

                    Console.WriteLine(artist.SearchName);

                    artistRepository.Update(artist);

                    Console.WriteLine(counter);

                    WriteColor("\nOk.", ConsoleColor.Green);
                }
                catch (Exception ex)
                {
                    _logger.ErrorException(string.Format("Error. {0}", ex.Message), ex);
                    WriteColor(ex.Message, ConsoleColor.Red);
                }
            }
        }

        private static void GenerateGenreSearchName()
        {
            var artistRepository = new MongoRepository.MongoRepository<Models.Genre>();

            foreach (var genre in artistRepository.All())
            {
                var parts = GenerateSearchParts(genre.Name);

                genre.SearchName = ConvertListSearchWordToSearchName(parts);

                artistRepository.Update(genre);
                
                Console.WriteLine("OK");
            }
        }

        private static void RecalcTrackCount()
        {
            var artistRepository = new MongoRepository.MongoRepository<Models.Artist>();


            int counter = 0;
            foreach (var artist in artistRepository.All())
            {
                try
                {
                    artist.TrackCount = artist.Tracks.Count;
                    artistRepository.Update(artist);
                }
                catch (Exception ex)
                {
                    _logger.ErrorException(string.Format("RecalcTrackCount. error: {0}", ex.Message), ex);
                    Console.WriteLine(ex.Message);
                }

                Console.Clear();
                Console.Write("Counter: {0}", ++counter);
            }
        }

        private static IEnumerable<string> GenerateSearchParts(string searchName)
        {
            if (string.IsNullOrEmpty(searchName) || searchName.Length <= 4)
            {
                return new List<string>
                           {
                               searchName
                           };
            }

            var list = new List<string>();

            for (var i = 3; i <= searchName.Length - 1; i++)
            {
                list.Add(searchName.Substring(0, i));
            }

            list.Add(searchName);

            return list;
        }

        private static string ConvertListSearchWordToSearchName(IEnumerable<string> words)
        {
            var sb = new StringBuilder();

            foreach (var word in words)
            {
                sb.AppendFormat("{0} ", word);
            }

            return sb.ToString();
        }

        private static List<string> GenerateSearchNameFromArtistMB(Artist artist)
        {
            var list = new List<string>();

            foreach (var artistName in artist.ArtistCreditNamesRef)
            {
                list.AddRange(GenerateSearchParts(artistName.ArtistNameRef.Name.ToLower()));
            }

            foreach (var artistSortName in artist.ArtistSortNameRef.ArtistCreditNameRef)
            {
                list.AddRange(GenerateSearchParts(artistSortName.ArtistNameRef.Name.ToLower()));
            }

            return list.Distinct().ToList();
        }

        private static string ClearName(string name)
        {
            return name.ToLower();
        }

        private static void ShowArtistAlbums()
        {
            using (var context = new MusicBrainzModel())
            {
                var artist = context.Artists.FirstOrDefault(x => x.Id == 1694);

                if (artist == null)
                    return;

                foreach (var cnames in artist.ArtistCreditNamesRef)
                {
                    Console.WriteLine(cnames.ArtistNameRef.Name);

                    var release_name = string.Empty;
                    foreach (var recording in cnames.ArtistCreditRef.Recordings)
                    {
                        var track = recording.Tracks.FirstOrDefault();
                        if (track == null)
                            continue;

                        var media = track.TracklistRef.Media.FirstOrDefault();
                        if (media == null)
                            continue;

                        if (release_name != media.ReleaseRef.ReleaseNameRef.Name)
                        {
                            Console.WriteLine("\t{0}", media.ReleaseRef.ReleaseNameRef.Name);
                            release_name = media.ReleaseRef.ReleaseNameRef.Name;
                        }

                        Console.WriteLine("\t\t{0}", recording.TrackNameRef.Name);
                    }
                }
            }
        }

        private static string ClearBio(string bio)
        {
            return bio.Replace(
                "User-contributed text is available under the Creative Commons By-SA License and may also be available under the GNU FDL.",
                string.Empty);
        }

        private static void WriteColor(string text, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(text);
            Console.ResetColor();
        }
    }
}
