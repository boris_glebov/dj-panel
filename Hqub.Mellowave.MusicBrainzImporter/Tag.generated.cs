#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Data.Common;
using System.Collections.Generic;
using Telerik.OpenAccess;
using Telerik.OpenAccess.Metadata;
using Telerik.OpenAccess.Data.Common;
using Telerik.OpenAccess.Metadata.Fluent;
using Telerik.OpenAccess.Metadata.Fluent.Advanced;
using Hqub.Mellowave.MusicBrainzImporter;


namespace Hqub.Mellowave.MusicBrainzImporter	
{
	public partial class Tag
	{
		private int _id;
		public virtual int Id 
		{ 
		    get
		    {
		        return this._id;
		    }
		    set
		    {
		        this._id = value;
		    }
		}
		
		private string _name;
		public virtual string Name 
		{ 
		    get
		    {
		        return this._name;
		    }
		    set
		    {
		        this._name = value;
		    }
		}
		
		private int _ref_count;
		public virtual int Ref_count 
		{ 
		    get
		    {
		        return this._ref_count;
		    }
		    set
		    {
		        this._ref_count = value;
		    }
		}
		
		private IList<Artist_tag> _artist_tags = new List<Artist_tag>();
		public virtual IList<Artist_tag> Artist_tags 
		{ 
		    get
		    {
		        return this._artist_tags;
		    }
		}
		
		private IList<Recording_tag> _recording_tags = new List<Recording_tag>();
		public virtual IList<Recording_tag> RecordingTagsRef 
		{ 
		    get
		    {
		        return this._recording_tags;
		    }
		}
		
	}
}
