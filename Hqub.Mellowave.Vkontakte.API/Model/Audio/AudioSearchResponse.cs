﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace Hqub.Mellowave.Vkontakte.API.Model.Audio
{
    [XmlRoot("response")]
    public class AudioSearchResponse : Entity
    {
        [XmlAttribute("list")]
        public bool IsList { get; set; }

        [XmlElement("count")]
        public int Count { get; set; }

        [XmlElement("audio")]
        public List<Audio> Tracks { get; set; } 

    }
}
