﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Hqub.Mellowave.Vkontakte.API.Model.Audio
{
    [XmlRoot("audio")]
    public class Audio
    {
        [XmlElement("aid")]
        public string Id { get; set; }

        [XmlElement("owner_id")]
        public string OwnerId { get; set; }

        [XmlElement("title")]
        public string Title { get; set; }

        [XmlElement("artist")]
        public string Artist { get; set; }

        [XmlElement("duration")]
        public int Duration { get; set; }

        [XmlElement("url")]
        public string Url { get; set; }
    }
}
