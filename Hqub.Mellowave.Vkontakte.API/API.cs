﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hqub.Mellowave.Vkontakte.API.Model;
using Hqub.Mellowave.Vkontakte.API.Model.Audio;

namespace Hqub.Mellowave.Vkontakte.API
{
    public class API
    {
        public API(string token)
        {
            Token = token;
        }

        public AudioSearchResponse SearchAudio(string query, int count=50, int offset=0)
        {
            var response = RequestHelper.GetMethod<AudioSearchResponse>("audio.search",
                                                                                    string.Format("q={0}&count={1}&offset={2}", query, count, offset), Token);

            return response;
        }

        public Audio GetAudio(string trackId)
        {
            var response = RequestHelper.GetMethod<AudioSearchResponse>("audio.getById", string.Format("audios={0}", trackId),
                                                                      Token);

            return response != null && response.Tracks.Count > 0 ? response.Tracks[0] : null;
        }

        public ProfileResponse GetProfile(string uid)
        {
            var response = RequestHelper.GetMethod<ProfileResponse>("users.get", string.Format("uids={0}&fields=photo_100", uid), Token);

            return response;
        }

        public void SetStatus(string message, string audio)
        {
            var response = RequestHelper.GetMethod("status.set", string.Format("text={0}&audio={1}", message, audio),
                                                   Token);

            System.Diagnostics.Debug.WriteLine(response);
        }

        private string Token { get; set; }

        public bool IsTokenSet { get { return !string.IsNullOrEmpty(Token); } }
    }
}
