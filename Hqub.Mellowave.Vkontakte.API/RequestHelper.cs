﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace Hqub.Mellowave.Vkontakte.API
{
    public static class RequestHelper
    {
        public static T Get<T>(string url)
        {
            try
            {
                var request = System.Net.WebRequest.Create(url);

                var response = request.GetResponse();

                using (var stream = response.GetResponseStream())
                {
                    if (stream == null)
                        throw new NullReferenceException(Localization.Messages.StreamIsEmpty);

                    var xml = new XmlSerializer(typeof (T));
                    return (T) xml.Deserialize(stream);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Unknow result. " + ex.Message);
            }
        }

        public static string Get(string url)
        {
            try
            {
                var request = System.Net.WebRequest.Create(url);

                var response = request.GetResponse();

                using (var stream = response.GetResponseStream())
                {
                    if (stream == null)
                        throw new NullReferenceException(Localization.Messages.StreamIsEmpty);

                    using (var streamReader = new StreamReader(stream))
                    {
                        return streamReader.ReadToEnd();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Unknow result. " + ex.Message);
            }
        }

        public static T GetMethod<T>(string methodName, string methodParametrs, string accessToken)
        {
            return Get<T>(string.Format(Properties.Settings.Default.UrlMethod, methodName, methodParametrs, accessToken));
        }

        public static string GetMethod(string methodName, string methodParametrs, string accessToken)
        {
            return Get(string.Format(Properties.Settings.Default.UrlMethod, methodName, methodParametrs, accessToken));
        }
    }
}
