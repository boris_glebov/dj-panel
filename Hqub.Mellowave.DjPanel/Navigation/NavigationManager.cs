﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hqub.Mellowave.DjPanel.Modules;

namespace Hqub.Mellowave.DjPanel.Navigation
{
    public delegate void NavigationManagerStateChanged(NavigationManager sender);

    public class NavigationManager
    {
        Stack<BaseNavigationModule> _modulesStack = new Stack<BaseNavigationModule>();

        public event NavigationManagerStateChanged NavigationManagerStateChanged;

        public void GoNext(BaseNavigationModule module)
        {
            if (_modulesStack.Count > 0)
            {
                var currentModule = _modulesStack.Peek();
                currentModule.BeforeNext();
                currentModule.Hide();
            }

            module.Show();

            _modulesStack.Push(module);

            if (NavigationManagerStateChanged != null)
                NavigationManagerStateChanged(this);
        }

        public void GoBack(BaseNavigationModule module = null)
        {
            if (module == null)
            {
                if (_modulesStack.Count > 1)
                {
                    var currentModule = _modulesStack.Pop();
                    currentModule.BeforeBack();
                    currentModule.Hide();
                    _modulesStack.Peek().Show();
                }
            }
            else if (_modulesStack.Contains(module))
            {
                if (_modulesStack.Peek() != module)
                {
                    var currentModule = _modulesStack.Pop();
                    currentModule.BeforeBack();
                    currentModule.Hide();

                    while (_modulesStack.Peek() != module)
                    {
                        currentModule = _modulesStack.Pop();
                        currentModule.BeforeBack();
                    }

                    module.Show();
                }
            }

            if (NavigationManagerStateChanged != null)
                NavigationManagerStateChanged(this);
        }

        public void GoTo(Type moduleType)
        {
            var module =
                _modulesStack.FirstOrDefault(x => x.GetType().AssemblyQualifiedName == moduleType.AssemblyQualifiedName);

            if (module == null)
            {
                return;
            }

            GoNext(module);
        }

        public bool ExitsModule(Type moduleType)
        {
            return _modulesStack.Any(x => x.GetType().AssemblyQualifiedName == moduleType.AssemblyQualifiedName);
        }

        public IEnumerable<BaseNavigationModule> GetModulesStack()
        {
            var modules = new BaseNavigationModule[_modulesStack.Count];
            _modulesStack.CopyTo(modules, 0);
            return modules.Reverse();
        }
    }
}
