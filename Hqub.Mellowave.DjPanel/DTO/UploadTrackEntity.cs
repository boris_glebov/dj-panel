﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Hqub.Mellowave.DjPanel.DTO
{
    public class UploadTrackEntity
    {
        public UploadTrackEntity()
        {

        }

        public UploadTrackEntity(byte[] file)
        {
            File = Convert.ToBase64String(file);
        }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("artist")]
        public string Artist { get; set; }

        [JsonProperty("file")]
        public string File { get; set; }
    }
}
