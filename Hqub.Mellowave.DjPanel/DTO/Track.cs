﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Hqub.Mellowave.DjPanel.DTO
{
    public partial class Track : Entity
    {
        private int _duration;
        private string _title;
        private string _artist;
        private int _bitrate;
        private ObservableCollection<Artist> _artists;
        private ObservableCollection<string> _artistImages;

        #region DTO Properties

        [JsonProperty("vkid")]
        public string VkId { get; set; }

        [JsonProperty("mbid")]
        public string MBId { get; set; }

        [JsonProperty("hash")]
        public string Hash { get; set; }

        [JsonProperty("title")]
        public string Title
        {
            get { return _title; }
            set
            {
                _title = value;
                RaisePropertyChanged();
                RaisePropertyChanged("FullName");
            }
        }

        [JsonProperty("artist")]
        public string Artist
        {
            get { return _artist; }
            set
            {
                _artist = value;
                RaisePropertyChanged();
            }
        }

        [JsonProperty("artists")]
        public ObservableCollection<Artist> Artists
        {
            get { return _artists; }
            set
            {
                if (Equals(value, _artists)) return;
                _artists = value;
                RaisePropertyChanged();
            }
        }

        [JsonProperty("artist_images")]
        public ObservableCollection<string> ArtistImages
        {
            get { return _artistImages; }
            set
            {
                if (Equals(value, _artistImages)) return;
                _artistImages = value;
                RaisePropertyChanged();
            }
        }

        [JsonProperty("statistics")]
        public TrackStatistic Statistic { get; set; }

        [JsonProperty("duration")]
        public int Duration
        {
            get { return _duration; }
            set
            {
                _duration = value;
                RaisePropertyChanged();
                RaisePropertyChanged("FullName");
            }
        }

        [JsonProperty("bitrate")]
        public int Bitrate
        {
            get { return _bitrate; }
            set
            {
                _bitrate = value;
                RaisePropertyChanged();
                RaisePropertyChanged("BitrateString");
            }
        }


//        [JsonProperty("playlists")]
//        public ObservableCollection<string> Playlists { get; set; }

        #endregion

    }
}
