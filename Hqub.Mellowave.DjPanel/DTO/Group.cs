﻿using Hqub.Mellowave.DjPanel.Model;
using Newtonsoft.Json;

namespace Hqub.Mellowave.DjPanel.DTO
{
    public class Group : MongoEntity
    {
        [JsonProperty("group_id")]
        public string GroupId { get; set; }

        [JsonProperty("group_name")]
        public LanguageTranslator GroupName { get; set; }

        [JsonProperty("is_visible")]
        public bool IsVisible { get; set; }
    }
}
