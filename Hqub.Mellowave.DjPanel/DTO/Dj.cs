﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Hqub.Mellowave.DjPanel.Annotations;
using Newtonsoft.Json;

namespace Hqub.Mellowave.DjPanel.DTO
{
    public class Dj : INotifyPropertyChanged
    {
        private string _userId;
        private string _nick;
        private string _avatar;

        [JsonProperty("user_id")]
        public string UserId
        {
            get { return _userId; }
            set
            {
                if (value == _userId) return;
                _userId = value;
                OnPropertyChanged();
            }
        }

        [JsonProperty("nick")]
        public string Nick
        {
            get { return _nick; }
            set
            {
                if (value == _nick) return;
                _nick = value;
                OnPropertyChanged();
            }
        }

        [JsonProperty("avatar")]
        public string Avatar
        {
            get { return _avatar; }
            set
            {
                if (value == _avatar) return;
                _avatar = value;
                OnPropertyChanged();
            }
        }

        #region Implementated INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
