﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hqub.Mellowave.DjPanel.DTO
{
    public partial class Genre : MongoEntity
    {
        public string Name { get; set; }
    }
}
