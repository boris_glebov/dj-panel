﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Hqub.Mellowave.DjPanel.Model;
using Newtonsoft.Json;

namespace Hqub.Mellowave.DjPanel.DTO
{
    public partial class RadioStation : MongoEntity
    {
        private string _groupId;

        [JsonProperty("name")]
        public LanguageTranslator Name { get; set; }

        [JsonProperty("notes")]
        public string Notes { get; set; }

        [JsonProperty("station_id")]
        public string StationId { get; set; }

        [JsonProperty("tracks")]
        public ObservableCollection<Track> Tracks { get; set; }

        [JsonProperty("group_id")]
        public string GroupId
        {
            get { return _groupId; }
            set
            {
                _groupId = value;
                RaisePropertyChanged();
            }
        }

        [JsonProperty("tags")]
        public ObservableCollection<string> Tags { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("api_url")]
        public string ApiUrl { get; set; }

        [JsonProperty("tiles")]
        public List<string> Tiles { get; set; }

        [JsonProperty("station_type")]
        public string StationType { get; set; }

        [JsonProperty("dj")]
        public Dj Dj { get; set; }

        [JsonProperty("enabled")]
        public bool IsEnabled { get; set; }
    }
}
