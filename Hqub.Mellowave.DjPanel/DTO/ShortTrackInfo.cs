﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Hqub.Mellowave.DjPanel.DTO
{
    public class ShortTrackInfo
    {
        [JsonProperty("track_id")]
        public string TrackId { get; set; }

        [JsonProperty("track_name")]
        public string TrackName { get; set; }
    }
}
