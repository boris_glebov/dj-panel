﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Hqub.Mellowave.DjPanel.DTO
{
    public abstract class Entity
    {
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
