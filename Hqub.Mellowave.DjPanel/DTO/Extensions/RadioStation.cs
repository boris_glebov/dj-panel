﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using Hqub.Mellowave.DjPanel.Annotations;
using Newtonsoft.Json;

namespace Hqub.Mellowave.DjPanel.DTO
{
    public partial class RadioStation : INotifyPropertyChanged
    {
        public RadioStation()
        {
            Tiles = new List<string>();
            Tags = new ObservableCollection<string>();
            Dj = new Dj();
        }

        private bool _isPlays;

        [JsonIgnore]
        public bool IsPlays
        {
            get { return _isPlays; }
            set
            {
                _isPlays = value;
                RaisePropertyChanged();
            }
        }

        [JsonIgnore]
        public string RadioUrl
        {
            get { return string.Format("{0}{1}.mp3", Url, StationId); }
        }

        [JsonIgnore]
        public bool IsCreated { get; set; }

        #region Impelmentation INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
