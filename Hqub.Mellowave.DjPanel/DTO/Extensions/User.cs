﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hqub.Mellowave.DjPanel.DTO
{
    public partial class User
    {
        public string FullName
        {
            get { return string.Format("{0} {1}", LastName, FirstName); }
        }
    }
}
