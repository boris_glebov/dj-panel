﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using Hqub.Mellowave.DjPanel.Annotations;
using Newtonsoft.Json;
using System.Linq;

namespace Hqub.Mellowave.DjPanel.DTO
{
    public partial class Track : INotifyPropertyChanged, ICloneable, IEquatable<Track>
    {
        #region Fields

        private int _position;
        private int _totalDuration;
        private int _sliderVisibility;
        private int _sliderPosition;
        private bool _isPlays;
       
        private string _vkUrl;


        #endregion

        #region .ctor

        public Track()
        {
            SliderVisibility = 0;
            Statistic = new TrackStatistic();
        }

        #endregion

        #region Methods

        public void Update()
        {
            RaisePropertyChanged("IsDownloadedStatusVisible");
        }

        public void Fill(Track track)
        {
            SetProperties(track, this);
        }

        #endregion

        #region Служебные поля

        [JsonIgnore]
        public Visibility IsDownloadedStatusVisible
        {
            get { return IsDownloaded ? Visibility.Visible : Visibility.Hidden; }
        }

        [JsonIgnore]
        public Visibility ProgressBarVisibility
        {
            get
            {
                return IsPlays ? Visibility.Visible : Visibility.Hidden;
            }
            set
            {
                RaisePropertyChanged("ProgressBarVisibility");
            }
        }

        [JsonIgnore]
        public int SliderVisibility
        {
            get { return _sliderVisibility; }

            set
            {
                _sliderVisibility = value;
                RaisePropertyChanged("SliderVisibility");
            }
        }

        [JsonIgnore]
        public int SliderPosition
        {
            get { return _sliderPosition; }
            set
            {
                _sliderPosition = value;
                RaisePropertyChanged("SliderPosition");
            }
        }

        [JsonIgnore]
        public int TotalDuration
        {
            get { return _totalDuration; }
            set
            {
                _totalDuration = value;
                RaisePropertyChanged("TotalDuration");
            }
        }

        [JsonIgnore]
        public bool IsPlays
        {
            get { return _isPlays; }
            set
            {
                _isPlays = value;
                RaisePropertyChanged("IsPlays");
            }
        }

        [JsonIgnore]
        public int Position
        {
            get { return _position; }

            set
            {
                _position = value;
                Duration = value;
                RaisePropertyChanged("Position");
            }
        }

        [JsonIgnore]
        public bool IsDownloaded
        {
            get { return !string.IsNullOrEmpty(Hash); }
        }

        [JsonIgnore]
        public string FullName
        {
            get { return string.Format("{0} - {1}", Artist, Title); }
        }

        [JsonIgnore]
        public string VkUrl
        {
            get { return _vkUrl; }
            set
            {
                _vkUrl = value;
                RaisePropertyChanged("VkUrl");
            }
        }

        [JsonIgnore]
        public string BitrateString
        {
            get { return Bitrate == 0 ? string.Empty : string.Format("{0} кбит/с", Bitrate); }
        }

        
        #endregion

        #region Implementation INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        #region Implementation ICloneable

        public object Clone()
        {
            var track = new Track();

            SetProperties(this, track);

            track.ArtistImages = new ObservableCollection<string>();

            if(this.ArtistImages != null)
                foreach (var artistImage in this.ArtistImages)
                {
                    track.ArtistImages.Add(artistImage);
                }

            return track;
        }

        private void SetProperties(Track source, Track dest)
        {
            foreach (var property in typeof(Track).GetProperties().Where(x => x.CustomAttributes.All(a => a.AttributeType != typeof(JsonIgnoreAttribute))))
            {
                try
                {
                    property.SetValue(dest, property.GetValue(source));
                }
                catch (Exception exception)
                {
                    System.Diagnostics.Debug.WriteLine(exception.Message);
                }
            }
        }

        #endregion

        #region Implementation IEquatable<Track>

        public bool Equals(Track other)
        {
            //Check whether the compared object is null.
            if (Object.ReferenceEquals(other, null)) return false;

            //Check whether the compared object references the same data.
            if (Object.ReferenceEquals(this, other)) return true;

            //Check whether the products' properties are equal.
            return FullName.Equals(other.FullName);
        }

        public override int GetHashCode()
        {
            //Get hash code for the Name field if it is not null.
            int hashProductName = FullName == null ? 0 : FullName.GetHashCode();

           //Calculate the hash code for the product.
            return hashProductName;
        }

        #endregion

    }
}
