﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Hqub.Mellowave.DjPanel.DTO
{
    public partial class User : MongoEntity
    {
        [JsonProperty(PropertyName = "first_name")]
        public string FirstName { get; set; }

        [JsonProperty(PropertyName = "last_name")]
        public string LastName { get; set; }

        [JsonProperty(PropertyName = "user_id")]
        public string UserId { get; set; }
//
//        [JsonProperty(PropertyName = "last_visit")]
//        public DateTime LastVisit { get; set; }

        [JsonProperty(PropertyName = "owns")]
        public ObservableCollection<string> Owns { get; set; } 
    }
}
