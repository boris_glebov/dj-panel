﻿using Newtonsoft.Json;

namespace Hqub.Mellowave.DjPanel.DTO.Response
{
    public class StationSaveResponse : BaseResponse
    {
        [JsonProperty("hash")]
        public string Hash { get; set; }
    }
}
