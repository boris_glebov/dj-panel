﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Hqub.Mellowave.DjPanel.DTO.Response
{
    public class UploadTrackResponse : BaseResponse
    {
        [JsonProperty("bitrate")]
        public int Bitrate { get; set; }
    }
}
