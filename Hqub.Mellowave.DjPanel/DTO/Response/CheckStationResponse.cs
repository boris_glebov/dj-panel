﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Hqub.Mellowave.DjPanel.DTO.Response
{
    public class CheckStationResponse : BaseResponse
    {
        [JsonProperty("not_exists_count")]
        public int NotExistsTrackCount { get; set; }
    }
}
