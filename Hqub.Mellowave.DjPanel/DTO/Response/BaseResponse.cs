﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Hqub.Mellowave.DjPanel.DTO.Response
{
    public class BaseResponse
    {
        [JsonProperty("error")]
        public string Error { get; set; }

        [JsonProperty("code")]
        public int Code { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonIgnore]
        public bool IsError
        {
            get { return Status != "success" || !string.IsNullOrEmpty(Error); }
        }
    }
}
