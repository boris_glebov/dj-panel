﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hqub.Mellowave.DjPanel.Model;
using MongoDB.Bson;
using Newtonsoft.Json;

namespace Hqub.Mellowave.DjPanel.DTO
{
    public abstract class MongoEntity : Entity
    {
        [JsonIgnore]
        public BsonObjectId Id
        {
            get { return new ObjectId(Oid.Value); }
        }

        [JsonProperty("_id")]
        public PythonObjectId Oid { get; set; }
    }
}
