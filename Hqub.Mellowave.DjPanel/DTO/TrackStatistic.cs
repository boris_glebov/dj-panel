﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Hqub.Mellowave.DjPanel.DTO
{
    public class TrackStatistic
    {
        [JsonProperty("votes")]
        public int Votes { get; set; }

        [JsonProperty("favs")]
        public int Favs { get; set; }

        [JsonProperty("ban")]
        public int Ban { get; set; }
    }
}
