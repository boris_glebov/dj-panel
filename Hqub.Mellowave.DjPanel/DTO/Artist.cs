﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Hqub.Mellowave.DjPanel.Annotations;
using Newtonsoft.Json;

namespace Hqub.Mellowave.DjPanel.DTO
{
    public class Artist : INotifyPropertyChanged
    {
        private string _name;
        private string _mbId;
        private ObservableCollection<string> _images;

        [JsonProperty("name")]
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                RaiseProprertyChanged();
            }
        }

        [JsonProperty("mbid")]
        public string MBId
        {
            get { return _mbId; }
            set
            {
                if (value == _mbId) return;
                _mbId = value;
                RaiseProprertyChanged();
            }
        }

        [JsonProperty("images")]
        public ObservableCollection<string> Images
        {
            get { return _images; }
            set
            {
                if (Equals(value, _images)) return;
                _images = value;
                RaiseProprertyChanged();
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void RaiseProprertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
