﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Navigation;

namespace Hqub.Mellowave.DjPanel.Behaviors
{
    public class WebBrowserExtendedBehavior : Behavior<WebBrowser>
    {
        public static readonly DependencyProperty RedirectCommandProperty =
            DependencyProperty.Register("RedirectCommand", typeof (ICommand), typeof (WebBrowserExtendedBehavior), new PropertyMetadata(default(ICommand)));

        public ICommand RedirectCommand
        {
            get { return (ICommand) GetValue(RedirectCommandProperty); }
            set { SetValue(RedirectCommandProperty, value); }
        }

        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.Navigated +=AssociatedObject_Navigated;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            AssociatedObject.Navigated -= AssociatedObject_Navigated;
        }

        private void AssociatedObject_Navigated(object sender, NavigationEventArgs e)
        {
            if(RedirectCommand != null)
                RedirectCommand.Execute(e);
        }
    }
}
