﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Windows.Controls;

namespace Hqub.Mellowave.DjPanel.Behaviors
{
    public class GenreFilteringBehavior : FilteringBehavior
    {
        public override IEnumerable<object> FindMatchingItems(string searchText, System.Collections.IList items, IEnumerable<object> escapedItems, string textSearchPath, TextSearchMode textSearchMode)
        {
            var result = base.FindMatchingItems(searchText, items, escapedItems, textSearchPath, textSearchMode);

            var findMatchingItems = result as object[] ?? result.ToArray();
            if (!string.IsNullOrEmpty(searchText) && !findMatchingItems.Any())
            {
              return  items.Cast<DTO.Genre>().Where(x => x.Name.ToLower() == searchText.ToLower());
            }

            return findMatchingItems;
        }
    }
}
