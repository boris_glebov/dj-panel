﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Hqub.Mellowave.DjPanel.Converters
{
    public class DurationConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var duration = (int) value;

            var t = TimeSpan.FromSeconds(duration);

            if (t.Hours == 0)
                return string.Format("{0:D2}:{1:D2}", t.Minutes, t.Seconds);


            return t.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
