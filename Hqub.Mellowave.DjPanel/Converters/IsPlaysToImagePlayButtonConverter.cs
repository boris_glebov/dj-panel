﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Hqub.Mellowave.DjPanel.Converters
{
    public class IsPlaysToImagePlayButtonConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var isPlays = (bool) value;

            return isPlays ? new Uri(@"\Content\metro-black\pause.png", UriKind.Relative) : 
                             new Uri(@"\Content\metro-black\play.png", UriKind.Relative);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
