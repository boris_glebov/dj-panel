﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using Microsoft.Practices.Prism.ViewModel;

namespace Hqub.Mellowave.DjPanel.Model
{
    public abstract class Entity : Microsoft.Practices.Prism.ViewModel.NotificationObject, INotifyDataErrorInfo
    {
        private ErrorsContainer<ValidationResult> _errorsContainer;

        protected Entity()
        {
            _errorsContainer = new ErrorsContainer<ValidationResult>(RaiseErrorsChanged);
        }

        public IEnumerable GetErrors(string propertyName)
        {
            return _errorsContainer.GetErrors(propertyName);
        }

        public bool HasErrors
        {
            get { return this._errorsContainer.HasErrors; }
        }

        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

        protected void RaiseErrorsChanged(string propertyName)
        {
            var handler = this.ErrorsChanged;
            if(handler != null)
            {
                handler(this, new DataErrorsChangedEventArgs(propertyName));
            }
        }
    }
}
