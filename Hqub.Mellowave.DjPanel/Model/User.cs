﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hqub.Mellowave.DjPanel.Model
{
    public class User : Entity
    {

        #region .ctor

        public User(string clientId, string accessToken)
        {
            UserId = clientId;
            AccessToken = accessToken;
        }

        #endregion

        #region Properties

        public string UserId { get; set; }

        public string AccessToken { get; set; }

        #endregion


        #region Calculate Properties

      

        #endregion
    }
}
