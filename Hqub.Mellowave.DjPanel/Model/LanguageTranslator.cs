﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Hqub.Mellowave.DjPanel.Model
{
    public class LanguageTranslator
    {
        [JsonProperty("eng")]
        public string Eng { get; set; }


        [JsonProperty("ru")]
        public string Rus { get; set; }

        public override string ToString()
        {
            return Eng;
        }
    }
}
