﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Hqub.Mellowave.DjPanel.Model
{
    public class PythonObjectId
    {
        public PythonObjectId()
        {
            
        }

        public PythonObjectId(string value)
        {
            Value = value;
        }

        public PythonObjectId(MongoDB.Bson.ObjectId objectId)
        {
            Value = objectId.ToString();
        }

        [JsonProperty("$oid")]
        public string Value { get; set; }

        public override string ToString()
        {
            return Value;
        }
    }
}
