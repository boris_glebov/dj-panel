﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DotLastFm.Api;

namespace Hqub.Mellowave.DjPanel.Model
{
    public class LastfmConfigEntity : ILastFmConfig
    {
        /// <summary>
        /// Gets the base Last.fm's URL.
        /// </summary>
        public string BaseUrl
        {
            get
            {
                return "http://ws.audioscrobbler.com/2.0";
            }
        }

        /// <summary>
        /// Gets the Last.fm's API key.
        /// </summary>
        public string ApiKey
        {
            get
            {
                return "a012acc1e5f8a61bc7e58238ce3021d8";
            }
        }

        /// <summary>
        /// Gets the Last.fm's secret.
        /// </summary>
        public string Secret
        {
            get
            {
                return "86776d4f43a72633fb37fb28713a7798";
            }
        }
    }
}
