﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hqub.Mellowave.DjPanel
{
    public static class RegionNames
    {
        public static string MainRegionName = "MainRegion";
        public static string DashboardContentRegionName = "DashboardContentRegion";
        public static string RadioPanelControl = "RadioPanelControl";
    }
}
