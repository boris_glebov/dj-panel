﻿using System.Windows;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.UnityExtensions;
using Microsoft.Practices.Unity;
using Telerik.Windows.Controls;

namespace Hqub.Mellowave.DjPanel
{
    public class Bootstrap : UnityBootstrapper
    {
        protected override DependencyObject CreateShell()
        {
            //Устанавливаем дефолтную тему:
            StyleManager.ApplicationTheme = new Windows8Theme();

            var main = Container.Resolve<MainWindowView>();

            main.Show();

            return main;
        }

        protected override void ConfigureModuleCatalog()
        {

            ModuleCatalog.AddModule(new ModuleInfo
                                        {
                                            ModuleName = "AuthModule",
                                            ModuleType =
                                                typeof (Modules.AuthModule)
                                                .AssemblyQualifiedName,
                                            InitializationMode =
                                                InitializationMode.OnDemand
                                        });

            ModuleCatalog.AddModule(new ModuleInfo
                                        {
                                            ModuleName = "MainModule",
                                            ModuleType =
                                                typeof (Modules.MainModule)
                                                .AssemblyQualifiedName,
                                            InitializationMode =
                                                InitializationMode.OnDemand
                                        });

            ModuleCatalog.AddModule(new ModuleInfo
                                        {
                                            ModuleName = "RadioModule",
                                            ModuleType =
                                                typeof (Modules.RadioModule)
                                                .AssemblyQualifiedName,
                                            InitializationMode =
                                                InitializationMode.OnDemand
                                        });
        }

        protected override void ConfigureContainer()
        {
            Container.RegisterInstance(Container);
            Container.RegisterInstance(new Navigation.NavigationManager());
            Container.RegisterInstance(new ServiceClient());
            Container.RegisterInstance(new Utilities.Navigation());
            Container.RegisterInstance(new Utilities.GlobalTimer());
            
            // Регистрируем View:
            Container.RegisterType(typeof (View.Auth.AuthView));
            Container.RegisterType(typeof (View.Main.DashboardView));
            Container.RegisterType(typeof (View.Dialogs.SearchAudioDialogView));
            Container.RegisterType(typeof (View.Radio.RadioStationsView));
            Container.RegisterType(typeof (View.Radio.RadioPanelView));
            Container.RegisterType(typeof (View.Dialogs.StationGroupListView));
            Container.RegisterType(typeof (View.Radio.TrackView));
            Container.RegisterType(typeof (View.Radio.RadioSettingsDialogView));
            Container.RegisterType(typeof (View.Dialogs.GenerateFishByGenres));

            // Регистрируем ViewModel:
            Container.RegisterType(typeof (ViewModel.MainWindowViewModel));
            Container.RegisterType(typeof (ViewModel.Auth.AuthViewModel));
            Container.RegisterType(typeof (ViewModel.Main.DashboardViewModel));
            Container.RegisterType(typeof (ViewModel.Dialogs.SearchAudioDialogViewModel));
            Container.RegisterType(typeof (ViewModel.Radio.RadioStationsViewModel));
            Container.RegisterType(typeof (ViewModel.Radio.RadioPanelViewModel));
            Container.RegisterType(typeof (ViewModel.Dialogs.StationGroupListViewModel));
            Container.RegisterType(typeof (ViewModel.Radio.TrackViewModel));
            Container.RegisterType(typeof (ViewModel.Radio.RadioSettingsDialogViewModel));
            Container.RegisterType(typeof (ViewModel.Dialogs.GenerateFishByGenresViewModel));

            base.ConfigureContainer();
        }
    }
}

