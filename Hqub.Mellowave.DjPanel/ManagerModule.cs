﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hqub.Mellowave.DjPanel.Modules;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.ServiceLocation;

namespace Hqub.Mellowave.DjPanel
{
    public static class ManagerModule
    {
        public static Modules.BaseNavigationModule LoadModule(Type module)
        {
            var m = (BaseNavigationModule) ServiceLocator.Current.GetInstance(module);
            m.Show();

            return m;
        }
    }
}
