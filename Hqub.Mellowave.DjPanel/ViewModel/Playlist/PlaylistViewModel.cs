﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using Hqub.Mellowave.DjPanel.DTO;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.ServiceLocation;

namespace Hqub.Mellowave.DjPanel.ViewModel.Playlist
{
    public class PlaylistViewModel : BaseViewModel
    {
        private bool _isBusy;
        private ObservableCollection<Track> _tracks;
        private Track _selectedTrack;
        private ListCollectionView _trackCollectionView;
        private string _searchText;

        public PlaylistViewModel(RadioStation station)
        {
            Station = station;
        }

        protected override void LoadCommandExecute(RoutedEventArgs args)
        {
            IsBusy = true;
            LoadPlaylist();
        }

        #region Methods

        private void LoadPlaylist()
        {
            try
            {
                var client = ServiceLocator.Current.GetInstance<ServiceClient>();

                var playlist = client.GetPlaylist(Station);

                Tracks = new ObservableCollection<Track>(playlist);
            }
            catch (Exception exception)
            {
                Logger.Main.ErrorException(string.Format("LoadPlaylist error. {0}", exception.Message), exception);
            }
            finally
            {
                IsBusy = false;
            }
        }

        private bool Filtering(object o)
        {
            var track = o as Track;
            if (track == null)
                return false;

            var checkResult = true;

            if (!string.IsNullOrEmpty(SearchText))
            {
                var lowerSearchText = SearchText.ToLower();
                checkResult &= track.Artist.ToLower().Contains(lowerSearchText) ||
                               track.Title.ToLower().Contains(lowerSearchText);
            }

            return checkResult;
        }

        #endregion

        #region Commands

        public ICommand BackCommand
        {
            get { return new DelegateCommand(BackCommandExecute); }
        }

        private void BackCommandExecute()
        {
            var navigation = ServiceLocator.Current.GetInstance<Utilities.Navigation>();
            navigation.Back(RegionNames.RadioPanelControl + Station.Name);
        }

        public ICommand SearchByEnterPressCommand
        {
            get { return new DelegateCommand<KeyEventArgs>(SearchByEnterPressCommandExecute); }
        }

        private void SearchByEnterPressCommandExecute(KeyEventArgs args)
        {
            switch (args.Key)
            {
                case Key.Enter:
                    if (string.IsNullOrEmpty(SearchText))
                        TrackCollectionView.Filter = null;
                    else
                        TrackCollectionView.Filter = Filtering;

                    args.Handled = true;

                    break;

                case Key.Escape:
                    SearchText = string.Empty;
                    args.Handled = true;

                    break;
            }
        }

        #endregion

        #region Properties

        public RadioStation Station { get; set; }

        #endregion

        #region Binding Properties

        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                if (value.Equals(_isBusy)) return;
                _isBusy = value;
                RaisePropertyChanged(() => IsBusy);
            }
        }

        public ObservableCollection<Track> Tracks
        {
            get { return _tracks; }
            set
            {
                if (Equals(value, _tracks)) return;
                _tracks = value;

                TrackCollectionView = new ListCollectionView(value);

                RaisePropertyChanged(() => Tracks);
                RaisePropertyChanged(() => TrackCount);
            }
        }

        public Track SelectedTrack
        {
            get { return _selectedTrack; }
            set
            {
                if (Equals(value, _selectedTrack)) return;
                _selectedTrack = value;
                RaisePropertyChanged(() => SelectedTrack);
            }
        }

        public string TrackCount
        {
            get
            {
                var trackCount = 0;
                if (Tracks != null)
                    trackCount = Tracks.Count;

                return string.Format("Кол-во трэков: {0}", trackCount);
            }
        }

        public ListCollectionView TrackCollectionView
        {
            get { return _trackCollectionView; }
            set
            {
                if (Equals(value, _trackCollectionView)) return;
                _trackCollectionView = value;
                RaisePropertyChanged(() => TrackCollectionView);
            }
        }

        public string SearchText
        {
            get { return _searchText; }
            set
            {
                if (value == _searchText) return;
                _searchText = value;

                if (string.IsNullOrEmpty(value))
                    TrackCollectionView.Filter = null;
                else
                {
                    TrackCollectionView.Filter = Filtering;
                }


                RaisePropertyChanged(() => SearchText);
            }
        }

        #endregion

 
    }
}
