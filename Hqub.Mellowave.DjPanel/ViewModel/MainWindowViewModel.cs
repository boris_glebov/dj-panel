﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Microsoft.Practices.ServiceLocation;

namespace Hqub.Mellowave.DjPanel.ViewModel
{
    public class MainWindowViewModel : BaseViewModel
    {
        #region Fields

        private Modules.AuthModule _authModule;

        #endregion

        #region .ctor

        public MainWindowViewModel()
        {
          
        }

        #endregion

        #region Methods

        protected override void LoadCommandExecute(RoutedEventArgs args)
        {
            ManagerModule.LoadModule(typeof(Modules.AuthModule));
        }

        #endregion

        #region Binding Properties

        public string Name { get { return "Hello"; } }

        #endregion
    }
}
