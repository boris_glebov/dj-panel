﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Hqub.Mellowave.DjPanel.DTO;
using Hqub.Mellowave.DjPanel.ViewModel;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.ServiceLocation;
using Telerik.Windows.Documents.RichTextBoxCommands;

namespace Hqub.Mellowave.DjPanel.ViewModel.Radio
{
    public class TrackViewModel : BaseViewModel
    {
        private Track _realTrack;
        private Track _track;
        private RadioStation _station;
        private string _selectedArtsitImage;
        private Artist _selectedArtist;

        public TrackViewModel(Track track, RadioStation station)
        {
            _realTrack = track;
            Track = (Track) track.Clone();

            Station = station;
        }

        #region Commands

        public ICommand BackCommand
        {
            get { return new DelegateCommand(BackCommandExecute); }
        }

        private void BackCommandExecute()
        {
            if (
                MessageBox.Show("Вернуться к списку трэков без сохранения?", "Внимание!", MessageBoxButton.YesNo,
                                MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.No)
                return;

            var navigation = ServiceLocator.Current.GetInstance<Utilities.Navigation>();
            navigation.Back(RegionNames.RadioPanelControl+Station.Name);
        }

        public ICommand OkCommand
        {
            get { return new DelegateCommand(OkCommandExecute); }
        }

        private void OkCommandExecute()
        {
            _realTrack.Fill(Track);

            CloseCommandExecute();
        }

        public ICommand CloseCommand
        {
            get { return new DelegateCommand(CloseCommandExecute); }
        }

        private void CloseCommandExecute()
        {
            var navigation = ServiceLocator.Current.GetInstance<Utilities.Navigation>();
            navigation.Back(RegionNames.RadioPanelControl + Station.Name);
        }

        #region Tab ArtistImage

        public ICommand AddArtistImageCommand { get { return new DelegateCommand(AddArtistImageCommandExecute); } }

        private void AddArtistImageCommandExecute()
        {
            var dialog = new View.Dialogs.AddArtistImageDialogView();
            if(dialog.ShowDialog() != true)
                return;

            if (Track.ArtistImages == null)
                Track.ArtistImages = new ObservableCollection<string>();

            Track.ArtistImages.Add(dialog.ArtistImageUrl);
        }

        public ICommand RemoveArtistImageCommand { get { return new DelegateCommand(RemoveArtistImageCommandExecute); } }

        private void RemoveArtistImageCommandExecute()
        {
            if(SelectedArtsitImage == null)
                return;

            Track.ArtistImages.Remove(SelectedArtsitImage);
            SelectedArtsitImage = Track.ArtistImages.FirstOrDefault();
        }

        #endregion

        #region Tab Artists

        public ICommand AddArtistCommand
        {
            get { return new DelegateCommand(AddArtistCommandExecute); }
        }

        private void AddArtistCommandExecute()
        {
            var dialog = new View.Dialogs.AddArtistToTrackDialogView();
            if (dialog.ShowDialog() == true)
            {
                AddArtist(dialog.Artist);
            }
        }

        public ICommand AddCurrentArtistCommand { get { return new DelegateCommand(AddCurrentArtistCommandExecute); } }

        private void AddCurrentArtistCommandExecute()
        {
            var dialog = new View.Dialogs.AddArtistToTrackDialogView(Track.Artist);
            if (dialog.ShowDialog() == true)
            {
               AddArtist(dialog.Artist);
            }
        }

        private void AddArtist(Artist artist)
        {
            if (Track.Artists == null)
                Track.Artists = new ObservableCollection<Artist>();

            Track.Artists.Add(artist);
        }

        public ICommand RemoveArtistCommand {get{return new DelegateCommand(RemoveArtistCommandExecute);}}

        private void RemoveArtistCommandExecute()
        {
           if(SelectedArtist == null)
               return;

            Track.Artists.Remove(SelectedArtist);

            SelectedArtist = Track.Artists.FirstOrDefault();
        }

        #endregion

        #endregion

        #region Binding Properties

        public Track Track
        {
            get { return _track; }
            set
            {
                if (Equals(value, _track)) return;
                _track = value;
                RaisePropertyChanged(() => Track);
            }
        }

        public RadioStation Station
        {
            get { return _station; }
            set
            {
                if (Equals(value, _station)) return;
                _station = value;
                RaisePropertyChanged(() => Station);
            }
        }

        public string SelectedArtsitImage
        {
            get { return _selectedArtsitImage; }
            set
            {
                if (value == _selectedArtsitImage) return;
                _selectedArtsitImage = value;
                RaisePropertyChanged(() => SelectedArtsitImage);
            }
        }

        public Artist SelectedArtist
        {
            get { return _selectedArtist; }
            set
            {
                if (Equals(value, _selectedArtist)) return;
                _selectedArtist = value;
                RaisePropertyChanged(() => SelectedArtist);
            }
        }

        #endregion
    }
}
