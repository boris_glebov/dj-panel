﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Hqub.Mellowave.DjPanel.DTO;
using Hqub.Mellowave.DjPanel.Utilities;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.ServiceLocation;

namespace Hqub.Mellowave.DjPanel.ViewModel.Radio
{
    public class RadioSettingsDialogViewModel : BaseDialogViewModel
    {
        private RadioStation _station;
        private string _stationName;
        private string _stationUrl;
        private string _notes;
        private Track _testStationTrack;
        private MediaPlayerWrapper _mediaPlayerWrapper;
        private bool _isBusy;
        private ObservableCollection<Uri> _tileUrls;
        private string _image1Url;
        private string _image2Url;
        private string _image3Url;
        private string _image4Url;
        private string _image5Url;
        private ObservableCollection<User> _moderators;
        private bool _isAccessPageBusy;
        private string _moderatorUserId;
        private string _tagName;
        private string _selectedTag;

        public RadioSettingsDialogViewModel(RadioStation station)
        {
            _mediaPlayerWrapper = new MediaPlayerWrapper();
            Station = station;

            if (station == null || station.StationId == null)
                return;

            SetupTiles(station.Tiles);
            GetModerators();
        }

        #region Tile Page

        #region Methods

        private void SetupTiles(List<string> tiles)
        {
            for (var i = 0; i < tiles.Count; i++)
            {
                var propName = string.Format("Image{0}Url", i + 1);
                var prop = this.GetType().GetProperty(propName);

                prop.SetValue(this, tiles[i]);

                RaisePropertyChanged(propName);
            }

            FillTileUrls();
        }

        private void FillTileUrls()
        {
            try
            {
                TileUrls = new ObservableCollection<Uri>
                               {
                                   new Uri(Image1Url),
                                   new Uri(Image2Url),
                                   new Uri(Image3Url),
                                   new Uri(Image4Url),
                                   new Uri(Image5Url)
                               };
            }
            catch (UriFormatException)
            {
                TileUrls = new ObservableCollection<Uri>();
            }
        }

        #endregion

        #region Binding Properties

        public ObservableCollection<Uri> TileUrls
        {
            get { return _tileUrls; }
            set
            {
                _tileUrls = value;
                RaisePropertyChanged("TileUrls");
            }
        }

        public string Image1Url
        {
            get { return _image1Url ?? string.Empty; }
            set
            {
                _image1Url = value;
                RaisePropertyChanged();
            }
        }

        public string Image2Url
        {
            get { return _image2Url ?? string.Empty; }
            set
            {
                _image2Url = value;
                RaisePropertyChanged("Image2Url");
            }
        }

        public string Image3Url
        {
            get { return _image3Url ?? string.Empty; }
            set
            {
                _image3Url = value;
                RaisePropertyChanged("Image3Url");
            }
        }

        public string Image4Url
        {
            get { return _image4Url ?? string.Empty; }
            set
            {
                _image4Url = value;
                RaisePropertyChanged(() => Image4Url);
            }
        }

        public string Image5Url
        {
            get { return _image5Url ?? string.Empty; }
            set
            {
                _image5Url = value;
                RaisePropertyChanged(() => Image5Url);
            }
        }

        #endregion

        #region Commands

        public ICommand AcceptTileChangedCommand { get { return new DelegateCommand(AcceptTileChangedCommandExecute); } }

        private void AcceptTileChangedCommandExecute()
        {
            try
            {
                FillTileUrls();

                Station.Tiles = new List<string>(TileUrls.Select(x => x.AbsoluteUri));

            }
            catch (Exception exception)
            {
                MessageBox.Show("Ошибка. Неверный формат ссылок.", "Ошибка");
                Logger.Main.ErrorException(
                    string.Format("Ошибка при обновление ссылок на тайлы. {0}", exception.Message), exception);
            }
        }

        #endregion

        #endregion

        #region Access Page

        #region Binding Properties

        public ObservableCollection<User> Moderators
        {
            get { return _moderators; }
            set
            {
                if (Equals(value, _moderators)) return;
                _moderators = value;
                RaisePropertyChanged(() => Moderators);
            }
        }

        public string ModeratorUserId
        {
            get { return _moderatorUserId; }
            set
            {
                if (value == _moderatorUserId) return;
                _moderatorUserId = value;
                RaisePropertyChanged(() => ModeratorUserId);
            }
        }

        #endregion

        #region Methods

        private async void GetModerators()
        {
            var serviceClient = ServiceLocator.Current.GetInstance<ServiceClient>();

            var result = await serviceClient.GetModeratorsAsync(Station);

            Moderators = new ObservableCollection<User>(result);
        }

        #endregion

        #region Commands

        public ICommand DedicateToModeratorsCommand
        {
            get { return new DelegateCommand(DedicateToModeratorsCommandExecute); }
        }

        private void DedicateToModeratorsCommandExecute()
        {
            IsAccessPageBusy = true;

            DedicateToModerators();
        }

        private async void DedicateToModerators()
        {
            try
            {
                var serviceClient = ServiceLocator.Current.GetInstance<ServiceClient>();

                var result = await serviceClient.DedicateToModeratorsAsync(ModeratorUserId, Station);

                if (result.IsError)
                {
                    MessageBox.Show(result.Error);
                    return;
                }

                GetModerators();
            }
            catch (Exception ex)
            {
                Logger.Main.ErrorException(string.Format("DedicateToModerators Exception. {0}", ex.Message), ex);
            }
            finally
            {
                IsAccessPageBusy = false;
                ModeratorUserId = string.Empty;
            }
        }

        #endregion

        #endregion

        #region Command

        public ICommand SaveCommand
        {
            get { return new DelegateCommand(SaveCommandExecute); }
        }

        private void SaveCommandExecute()
        {
            Station.Name.Rus = Station.Name.Eng = StationName;
            Station.Notes = Notes;

            if (!Check())
            {
                return;
            }

            var task = new Task(Save);

            IsBusy = true;

            task.Start();
        }

        private void Save()
        {
            try
            {
                var service = ServiceLocator.Current.GetInstance<ServiceClient>();

                // Некоторые новые станции создалсиь без типа. Эта временная строка исправит ошибку:
                Station.StationType = "expert";

                var result = service.SaveStation(Station);

                if (!result.IsError)
                {
                    Application.Current.Dispatcher.Invoke(CloseDialog);
                   
                    return;
                }

                Logger.Main.Error(string.Format("Station save: {0}", result.Error));
                MessageBox.Show(result.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("На сервере произошла ошибка. Обратитесь к администратору.", "Внутренняя ошибка.",
                                MessageBoxButton.OK, MessageBoxImage.Error);
                Logger.Main.ErrorException(string.Format("SavePlaylistCommand. {0}", ex.Message), ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        private bool Check()
        {
            if (string.IsNullOrEmpty(StationName) ||
                string.IsNullOrEmpty(Station.ApiUrl) ||
                string.IsNullOrEmpty(Station.Url) ||
                string.IsNullOrEmpty(Station.StationId)
                )
            {
                MessageBox.Show("Все поля отмеченные звездочкой обязательны к заполнению");
                return false;
            }

            return true;
        }

        public ICommand CancelCommand
        {
            get { return new DelegateCommand(CancelCommandExecute); }
        }

        private void CancelCommandExecute()
        {
            CloseDialog();
        }

        public ICommand CheckStationCommand {get {return new DelegateCommand(CheckStationCommandExecute);}}
        private void CheckStationCommandExecute()
        {
            if(_mediaPlayerWrapper.IsPlayed(TestStationTrack))
                _mediaPlayerWrapper.Stop(false);
            else
                _mediaPlayerWrapper.Play(TestStationTrack);
        }

        public ICommand SelectStationGroupCommand {get{return new DelegateCommand(SelectStationGroupCommandExecute);}}

        private void SelectStationGroupCommandExecute()
        {
            var dialog = ServiceLocator.Current.GetInstance<View.Dialogs.StationGroupListView>();

            dialog.ShowDialog();

            Station.GroupId = dialog.Group;
        }

        public ICommand AddTagCommand
        {
            get { return new DelegateCommand(AddTagCommandExecute); }
        }

        private void AddTagCommandExecute()
        {
            if(string.IsNullOrEmpty(TagName) || Station.Tags.Contains(TagName))
                return;

            Station.Tags.Add(TagName);

            TagName = string.Empty;
        }

        public ICommand AddTagByEnterPressCommand
        {
            get { return new DelegateCommand<KeyEventArgs>(AddTagByEnterPressCommandExecute); }
        }

        private void AddTagByEnterPressCommandExecute(KeyEventArgs args)
        {
            if (args.Key == Key.Escape)
            {
                TagName = string.Empty;
                return;
            }

            if (args.Key == Key.Return)
            {
                AddTagCommandExecute();
            }
        }

        public ICommand LoadAvatarFromVKCommand
        {
            get { return new DelegateCommand(LoadAvatarFromVKCommandExecute); }
        }

        private void LoadAvatarFromVKCommandExecute()
        {
            if (string.IsNullOrEmpty(Station.Dj.UserId))
            {
                MessageBox.Show("Для загрузки автарки, требуется знать ID пользователя.");
                return;
            }

            var api = new Vkontakte.API.API(Cache.Instance.GetIdentity().AccessToken);

            var response = api.GetProfile(Station.Dj.UserId);

            if (response == null || response.Profiles == null)
            {
                MessageBox.Show("Error VK API");
                return;
            }

            if (response.Profiles.Count == 0)
            {
                MessageBox.Show(string.Format("Пользователь '{0}' не найден.", Station.Dj.UserId));
                return;
            }

            var user = response.Profiles.First();

            Station.Dj.Avatar = user.Photo100;

        }

        public ICommand RemoveTagCommand{get{return new DelegateCommand<KeyEventArgs>(RemoveTagCommandExecute);}}

        private void RemoveTagCommandExecute(KeyEventArgs obj)
        {
            if (obj.Key != Key.Delete || string.IsNullOrEmpty(SelectedTag)) return;

            Station.Tags.Remove(SelectedTag);
            SelectedTag = Station.Tags.FirstOrDefault();
        }

        #endregion

        #region Methods

        private void CheckStringValue(string value, [CallerMemberName] string filedName = "")
        {
            if (string.IsNullOrEmpty(value))
                throw new NullReferenceException(string.Format("Поле '{0}' обязательно к заполнению", filedName));
        }

        protected override void ClosingCommandExecute(CancelEventArgs args)
        {
            base.ClosingCommandExecute(args);

            _mediaPlayerWrapper.Stop(false);
        }
        #endregion

        #region Binding Properties

        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                RaisePropertyChanged("IsBusy");
            }
        }

        public bool IsAccessPageBusy
        {
            get { return _isAccessPageBusy; }
            set
            {
                if (value.Equals(_isAccessPageBusy)) return;
                _isAccessPageBusy = value;
                RaisePropertyChanged(() => IsAccessPageBusy);
            }
        }

        public RadioStation Station
        {
            get { return _station; }
            set
            {
                _station = value;

                TestStationTrack = new Track
                {
                    VkUrl = _station.Url,
                    IsPlays = false
                };

                _stationName = value.Name.Rus;
                _stationUrl = value.Url;
                _notes = value.Notes;

                RaisePropertyChanged("Station");
            }
        }

        public string StationName
        {
            get { return _stationName; }
            set
            {
                _stationName = value;

                CheckStringValue(value);

                RaisePropertyChanged("StationName");
            }
        }

        public string Title
        {
            get { return string.Format("Настройка станции '{0}'", Station.Name); }
        }

        public string StationUrl
        {
            get { return _stationUrl; }
            set
            {
                _stationUrl = value;

                CheckStringValue(value);

                TestStationTrack.VkUrl = _stationUrl;

                RaisePropertyChanged("StationUrl");
            }
        }

        public string TagName
        {
            get { return _tagName; }
            set
            {
                if (value == _tagName) return;
                _tagName = value;
                RaisePropertyChanged(() => TagName);
            }
        }

        public bool IsStationIdEnabled { get { return string.IsNullOrEmpty(Station.StationId); } }

        public string Notes
        {
            get { return _notes; }
            set
            {
                _notes = value;

                CheckStringValue(value);

                RaisePropertyChanged("Notes");
            }
        }

        public Track TestStationTrack
        {
            get { return _testStationTrack; }
            set
            {
                _testStationTrack = value;
                RaisePropertyChanged("TestStationTrack");
            }
        }

        public string SelectedTag
        {
            get { return _selectedTag; }
            set
            {
                _selectedTag = value;
                RaisePropertyChanged("SelectedTag");
            }
        }

        public bool IsDisabled
        {
            get { return !Station.IsEnabled; }
            set
            {
                Station.IsEnabled = !value;
                RaisePropertyChanged("IsDisabled");
            }
        }

        #endregion

    }
}
