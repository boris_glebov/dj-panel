﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using Hqub.Mellowave.DjPanel.DTO;
using Hqub.Mellowave.DjPanel.Model;
using Hqub.Mellowave.DjPanel.Utilities;
using Hqub.Mellowave.DjPanel.ViewModel.Dialogs;
using Hqub.Mellowave.Vkontakte.API;
using Microsoft.Practices.Prism;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.ServiceLocation;
using ZuneWebApi;
using DelegateCommand = Microsoft.Practices.Prism.Commands.DelegateCommand;

namespace Hqub.Mellowave.DjPanel.ViewModel.Radio
{
    public class RadioPanelViewModel : BaseViewModel
    {
        private RadioStation _station;
        private Track _selectedTrack;
        private bool _isBusy;
        private bool _isSaveButtonEnabled = true;
        private bool _isUtilsMenuDropdownOpen;

        private LastFmApi.LastFm _lastFmApi;
        private Zune _zuneApi;

        #region .ctor

        public RadioPanelViewModel(RadioStation station)
        {
            Station = station;
            IsSortByNull = true;

            _mediaPlayerWrapper = new MediaPlayerWrapper();

            // Init Last and Zune API:
            var lastFmConfig = new LastfmConfigEntity();
            _lastFmApi = new LastFmApi.LastFm(lastFmConfig.ApiKey, lastFmConfig.Secret);

            _zuneApi = new Zune();

            GlobalCommands.GlobalTimerTick.RegisterCommand(TimerTickCommand);
        }

        #endregion

        #region Commands

        public ICommand InvokeSearchMusicCommand { get { return new DelegateCommand(InvokeSearchMusicExecute); } }

        private void InvokeSearchMusicExecute()
        {
            var searchAudioDialogView = new View.Dialogs.SearchAudioDialogView(new SearchAudioDialogViewModel());

            //ToDO: Баг. Закрыть приложение после поиска.
            var choosenTracks = searchAudioDialogView.ShowAndGetResults();

            if(choosenTracks == null)
                return;

            Tracks.AddRange(choosenTracks);

            RaisePropertyChanged("Tracks");
            RaisePropertyChanged("TracksCount");
            RaisePropertyChanged("CommonDuration");
        }

        #region PlayStop Command

        public ICommand PlayStopCommand
        {
            get { return new DelegateCommand<Track>(PlayStopCommandExecute); }
        }

        private void PlayStopCommandExecute(Track track)
        {
            if (track == null)
                return;

            try
            {
                track.VkUrl = GetTrackUrl(track.VkId);

                SetTrackToStatus(track.VkId);

                _mediaPlayerWrapper.Play(track);

                _mediaPlayerWrapper.MediaEnd += OnPlayTrackEnded;
            }
            catch (Exception exception)
            {
                Logger.Main.ErrorException(
                    string.Format("PlayStopCommandExecute. Id:{0} Url: {1}", track.VkId, track.VkUrl), exception);
            }
        }

        private void OnPlayTrackEnded(Track track)
        {
            var idx = Tracks.IndexOf(track);

            if (idx + 1 > Tracks.Count - 1)
                return;

            PlayStopCommandExecute(Tracks[idx + 1]);
        }

        private string GetTrackUrl(string trackId)
        {
            var vkApi = new API(Cache.Instance.GetIdentity().AccessToken);

            var track = vkApi.GetAudio(trackId);

            return track == null ? string.Empty : track.Url;
        }

        private void SetTrackToStatus(string trackId)
        {
            var vkApi = new API(Cache.Instance.GetIdentity().AccessToken);

            vkApi.SetStatus(string.Empty, trackId);
        }

        private void Save()
        {
            try
            {
                var service = ServiceLocator.Current.GetInstance<ServiceClient>();

                var result = service.SaveStation(Station);

                if (!result.IsError) return;

                Logger.Main.Error(string.Format("Station save: {0}", result.Error));
                MessageBox.Show(result.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("На сервере произошла ошибка. Обратитесь к администратору.", "Внутренняя ошибка.",
                                MessageBoxButton.OK, MessageBoxImage.Error);
                Logger.Main.ErrorException(string.Format("SavePlaylistCommand. {0}", ex.Message), ex);
            }
            finally
            {
                IsSaveButtonEnabled = true;
                IsBusy = false;
            }
        }


        #endregion


        #region Upload Command

        public ICommand UploadPlaylistCommand { get { return new DelegateCommand(UploadPlaylistCommandExecute); } }

        private async void UploadPlaylistCommandExecute()
        {
            var task = new Task(()=>Upload());

            IsBusy = true;
            IsSaveButtonEnabled = false;

            task.Start();

            await task;

            StatusMessage = string.Format("Обработка завершена.");
        }

        private void PrepareUpload()
        {
            CorrectArtistName(Station);

            FillArtistImages(Station.Tracks);
        }
        
        private void Upload(bool uploadAll=false)
        {
            var webClient = new WebClient();

            var vk = new API(Cache.Instance.GetIdentity().AccessToken);

            PrepareUpload();

            foreach (var track in Tracks.Where(x => string.IsNullOrEmpty(x.Hash) || uploadAll))
            {
                try
                {
                    var service = ServiceLocator.Current.GetInstance<ServiceClient>();

                    string trackUrl;

                    if (string.IsNullOrEmpty(track.VkUrl))
                    {
                        var audio = vk.GetAudio(track.VkId);
                        trackUrl = audio.Url;
                    }
                    else
                    {
                        trackUrl = track.VkUrl;
                    }

                    // Выставляем статус обрабатываемого файла:
                    StatusMessage = string.Format("Загружается: '{0}'", track.FullName);

                    var file = webClient.DownloadData(trackUrl);
                    if (file == null)
                        continue;

                    track.Hash = Vkontakte.API.Security.Md5Helper.GetMd5Hash(MD5.Create(), Encoding.UTF8.GetString(file));

                    var result = service.UploadTrack(Station, track, file);
                    // Если файл не закачался, то сбрасываем hash
                    if (result == null || result.IsError)
                    {
                        track.Hash = string.Empty;
                    }
                    else
                    {
                        track.Bitrate = result.Bitrate;
                        track.Update();
                    }
                   
                }
                catch (Exception downloadException)
                {
                    Logger.Main.ErrorException(string.Format("Не удалось скачать файл '{0}'.", track.VkUrl),
                                               downloadException);
                }
            }

            SavePlaylistCommandExecute();

            IsSaveButtonEnabled = true;
            IsBusy = false;
        }

        private void CorrectArtistName(RadioStation station)
        {
            foreach (var track in station.Tracks.Where(x => string.IsNullOrEmpty(x.MBId)))
            {
                try
                {
                    var info = _lastFmApi.ArtistGetInfo(track.Artist, string.Empty, true);

                    track.Artist = info.Name;
                    track.MBId = string.IsNullOrEmpty(info.Mbid) ? string.Empty : info.Mbid;
                }
                catch (Exception exception)
                {
                    Logger.Main.ErrorException(string.Format("Artist '{0}' getInfo error. {1}", track.Artist,
                                                             exception.Message), exception);
                }
            }
        }

        private void FillArtistImages(IEnumerable<Track> tracks)
        {
            foreach (var track in tracks)
            {
                try
                {
                    if (track.ArtistImages != null && track.ArtistImages.Count > 0 && track.Artists != null &&
                        track.Artists.Count > 0)
                        continue;

                    track.ArtistImages = ArtistImageSearchHelper.GetAtistImages(track.Artist);

                    if (track.Artists == null || track.Artists.Count == 0)
                    {
                        track.Artists = new ObservableCollection<Artist>();

                        FillArtists(track);
                    }
                }
                catch (Exception exception)
                {
                    Logger.Main.Warn(string.Format("FillArtistImages exception. {0}", exception.Message));
                }
            }
        }

        private void FillArtists(Track track)
        {
            var artist = _lastFmApi.ArtistGetInfo(track.Artist, string.Empty, true);

            if (artist == null)
                return;

            track.Artists.Add(new Artist
            {
                Name = artist.Name,
                MBId = artist.Mbid,
                Images = track.ArtistImages
            });
        }

        #endregion


        public ICommand ReloadFilesCommand { get { return new DelegateCommand(ReloadFilesCommandExecute); } }

        private void ReloadFilesCommandExecute()
        {
            if (MessageBox.Show("Удалить файлы", "Перегрузить все трэки на сервере?", MessageBoxButton.YesNo) ==
                MessageBoxResult.Yes)
            {
                var task = new Task(() => Upload(true));

                IsBusy = true;
                IsSaveButtonEnabled = false;

                task.Start();
            }
        }

        #region CheckStation Command

        public ICommand CheckStationCommand { get { return new DelegateCommand(CheckStationCommandExecute); } }

        private async void CheckStationCommandExecute()
        {
            IsBusy = true;
            IsUtilsMenuDropdownOpen = false;

            try
            {
                var response = await CheckStation();

                MessageBox.Show(string.Format("Кол-во отсусттвующих на сервере трэков = {0}",
                                              response.NotExistsTrackCount));
            }
            catch (Exception ex)
            {
                Logger.Main.ErrorException(string.Format("Error CheckStationCommandExecute. {0}", ex.Message), ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        private Task<DTO.Response.CheckStationResponse> CheckStation()
        {
            var service = ServiceLocator.Current.GetInstance<ServiceClient>();

            var task = new Task<DTO.Response.CheckStationResponse>(() => service.CheckStation(Station));

            task.Start();

            return task;
        }

        #endregion

        public ICommand PressKeyCommand { get { return new DelegateCommand<KeyEventArgs>(PressKeyCommandExecute); } }

        private void PressKeyCommandExecute(KeyEventArgs keyArgs)
        {
            if(keyArgs.Key == Key.Delete)
            {
                RemoveTrackCommand.Execute(SelectedTrack);
            }
        }


        public ICommand SettingStationCommand
        {
            get { return new DelegateCommand(SettingStationCommandExecute); }
        }

        private void SettingStationCommandExecute()
        {
            var dialog = new View.Radio.RadioSettingsDialogView(new RadioSettingsDialogViewModel(Station));
            dialog.ShowDialog();
        }


        public ICommand SearchByEnterPressCommand
        {
            get { return new DelegateCommand<KeyEventArgs>(SearchByEnterPressCommandExecute); }
        }

        private void SearchByEnterPressCommandExecute(KeyEventArgs args)
        {
            switch (args.Key)
            {
                case Key.Enter:
                    if (string.IsNullOrEmpty(SearchText))
                        TrackViewCollection.Filter = null;
                    else
                        TrackViewCollection.Filter = Filtering;

                    args.Handled = true;
                    
                    break;

                case Key.Escape:
                    SearchText = string.Empty;
                    args.Handled = true;
                    
                    break;
            }
        }


        public ICommand StartStationEtherCommand
        {
            get { return new DelegateCommand(StartStationEtherCommandExecute); }
        }

        private async void StartStationEtherCommandExecute()
        {
            var serviceClient = ServiceLocator.Current.GetInstance<ServiceClient>();

            try
            {
                IsBusy = true;

                var resposne = await serviceClient.StartStationEtherAsync(Station);

                Logger.Main.Info(string.Format("StartStationEther\n\t{0}", resposne));

                if (resposne.Contains("ERROR"))
                    MessageBox.Show(resposne);
            }
            catch (Exception exception)
            {
                Logger.Main.ErrorException(
                    string.Format("StartStationEtherCommandExecute error. {0}", exception.Message), exception);
            }
            finally
            {
                IsBusy = false;
            }
        }


        public ICommand StopStationEtherCommand
        {
            get { return new DelegateCommand(StopStationEtherCommandExecute); }
        }

        private async void StopStationEtherCommandExecute()
        {
            if (
                MessageBox.Show("Вы действительно хотите остановить вещание?", "Подтвердите остановку радио эфира.",
                                MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) ==
                MessageBoxResult.No)
                return;


            var serviceClient = ServiceLocator.Current.GetInstance<ServiceClient>();

            try
            {
                IsBusy = true;

                var resposne = await serviceClient.StopStationEtherAsync(Station);

                Logger.Main.Info(string.Format("StopStationEther\n\t{0}", resposne));

                if (resposne.Contains("ERROR"))
                    MessageBox.Show(resposne);
            }
            catch (Exception exception)
            {
                Logger.Main.ErrorException(
                    string.Format("StopStationEtherCommandExecute error. {0}", exception.Message), exception);
            }
            finally
            {
                IsBusy = false;
            }
        }


        public ICommand SkipTrackCommand { get { return new DelegateCommand(SkipTrackCommandExecute); } }

        private async void SkipTrackCommandExecute()
        {
            var serviceClient = ServiceLocator.Current.GetInstance<ServiceClient>();

            try
            {
                IsBusy = true;

                var resposne = await serviceClient.StationSkipCurrentTrackAsync(Station);

                Logger.Main.Info(string.Format("SkipTrackCommandExecute\n\t{0}", resposne));

                if (resposne.Contains("ERROR"))
                    MessageBox.Show(resposne);
            }
            catch (Exception exception)
            {
                Logger.Main.ErrorException(string.Format("SkipTrackCommandExecute error. {0}", exception.Message),
                                           exception);
            }
            finally
            {
                IsBusy = false;
            }
        }


        public ICommand TimerTickCommand { get { return new DelegateCommand(TimerTickCommandExecute); } }

        private async void TimerTickCommandExecute()
        {
            if (Station == null)
                return;

            var client = ServiceLocator.Current.GetInstance<ServiceClient>();
            NowPlaying = await client.GetNowPlyaingTrackAsync(Station);
        }


        #region Check and Update Command

        public ICommand CheckAndUpdateBitrateCommand { get { return new DelegateCommand(CheckAndUpdateBitrateCommandExecute); } }

        private async void CheckAndUpdateBitrateCommandExecute()
        {
            IsBusy = true;
            IsUtilsMenuDropdownOpen = false;

            await CheckAndUpdateBitrateAsync();
        }

        private Task CheckAndUpdateBitrateAsync()
        {
            var task = new Task(() =>
            {
                try
                {
                    var vk = new API(Cache.Instance.GetIdentity().AccessToken);

                    foreach (var track in Tracks.Where(x => x.Bitrate == 0))
                    {
                        var audio = vk.GetAudio(track.VkId);

                        if (audio == null)
                            continue;

                        track.Bitrate = MediaPlayerWrapper.GetBitrate(audio.Url, audio.Duration);
                    }
                }
                catch (Exception exception)
                {
                    Logger.Main.ErrorException(
                        string.Format("CheckAndUpdateBitrateAsync error. {0}", exception.Message),
                        exception);
                }
                finally
                {
                    IsBusy = false;
                }

            });

            task.Start();

            return task;
        }

        #endregion


        public ICommand ReloadStationCommand { get { return new DelegateCommand(ReloadStationCommandExecute); } }

        private async void ReloadStationCommandExecute()
        {
            try
            {
                IsBusy = true;
                var serviceClient = ServiceLocator.Current.GetInstance<ServiceClient>();

                await serviceClient.RealoadPlaylistAsync(Station);
            }
            catch (Exception exception)
            {
                Logger.Main.ErrorException(string.Format("ReloadStationCommand exception. {0}", exception.Message),
                                           exception);
            }
            finally
            {
                IsBusy = false;
            }

        }


        public ICommand RemoveTrackCommand
        {
            get { return new DelegateCommand(RemoveTrackCommandExecute); }
        }

        private void RemoveTrackCommandExecute()
        {
            if (SelectedTrack == null ||
                MessageBox.Show(string.Format("Удалить трэк '{0}' из плейлиста станции?", SelectedTrack.FullName),
                                "Подветрдите удаление трэка", MessageBoxButton.YesNo, MessageBoxImage.Question,
                                MessageBoxResult.No) ==
                MessageBoxResult.No)
                return;

            if (_mediaPlayerWrapper.IsPlayed(SelectedTrack))
                _mediaPlayerWrapper.Stop(false);

            Tracks.Remove(SelectedTrack);
            SelectedTrack = Tracks.FirstOrDefault();

            GlobalCommands.UpdateUI.Execute(null);

            RaisePropertyChanged("Tracks");
            RaisePropertyChanged("CommonDuration");
            RaisePropertyChanged("TracksCount");
        }


        public ICommand EditTrackNameCommand { get { return new DelegateCommand(EditTrackNameCommandExecute); } }

        private void EditTrackNameCommandExecute()
        {
            if (SelectedTrack == null)
            {
                return;
            }

            var navigation = ServiceLocator.Current.GetInstance<Utilities.Navigation>();

            navigation.Go(new Modules.TrackModule(SelectedTrack, Station), RegionNames.RadioPanelControl + Station.Name);
        }


        public ICommand SavePlaylistCommand
        {
            get { return new DelegateCommand(SavePlaylistCommandExecute); }
        }

        private void SavePlaylistCommandExecute()
        {
            var task = new Task(Save);

            IsBusy = true;
            IsSaveButtonEnabled = false;

            task.Start();
        }


        public ICommand ShowPlaylistCommand { get { return new DelegateCommand(ShowPlaylistCommandExecute); } }

        private void ShowPlaylistCommandExecute()
        {
            if (Station == null)
                return;

            var navigation = ServiceLocator.Current.GetInstance<Utilities.Navigation>();

            navigation.Go(new Modules.PlaylistModule(Station), RegionNames.RadioPanelControl + Station.Name);
        }


        public ICommand ClearHashTrackCommand { get { return new DelegateCommand(ClearHashTrackCommandExecute); } }

        private void ClearHashTrackCommandExecute()
        {
            if (
                MessageBox.Show("Очистить поле хэш у всех трэков?", "Очистка хэша", MessageBoxButton.YesNo,
                                MessageBoxImage.Warning, MessageBoxResult.No) == MessageBoxResult.No)
                return;

            foreach (var track in Station.Tracks)
            {
                track.Hash = string.Empty;
            }

            MessageBox.Show("Сохраните станцию.");
        }


        public ICommand GenerateFishForStationCommand
        {
            get { return new DelegateCommand(GenerateFishForStationExecute); }
        }

        private void GenerateFishForStationExecute()
        {
            var dialog = ServiceLocator.Current.GetInstance<View.Dialogs.GenerateFishByGenres>();

            dialog.ShowDialog();

            if(dialog.GeneratedTracks == null)
                return;

            Tracks.AddRange(dialog.GeneratedTracks);

            // Удаляем дубликаты
            RemoveDuplicateTracksCommandExecute();

            IsUtilsMenuDropdownOpen = false;
        }


        public ICommand RemoveDuplicateTracksCommand { get { return new DelegateCommand(RemoveDuplicateTracksCommandExecute); } }

        private void RemoveDuplicateTracksCommandExecute()
        {
            Station.Tracks = new ObservableCollection<Track>(Station.Tracks.Distinct());
            IsUtilsMenuDropdownOpen = false;
        }

        #endregion

        #region Methods

        private bool Filtering(object o)
        {
            var track = o as Track;
            if (track == null)
                return false;

            var checkResult = true;

            if (!string.IsNullOrEmpty(SearchText))
            {
                var lowerSearchText = SearchText.ToLower();
                checkResult &= track.Artist.ToLower().Contains(lowerSearchText) ||
                               track.Title.ToLower().Contains(lowerSearchText);
            }

            return checkResult;
        }


        #endregion

        #region Binding Properties

        public RadioStation Station
        {
            get { return _station; }
            set
            {
                _station = value;

                TrackViewCollection = new ListCollectionView(_station.Tracks);

                RaisePropertyChanged("Station");
            }
        }

        public bool IsUtilsMenuDropdownOpen
        {
            get { return _isUtilsMenuDropdownOpen; }
            set
            {
                if (value.Equals(_isUtilsMenuDropdownOpen)) return;
                _isUtilsMenuDropdownOpen = value;
                RaisePropertyChanged(() => IsUtilsMenuDropdownOpen);
            }
        }

        public Track SelectedTrack
        {
            get { return _selectedTrack; }
            set
            {
                _selectedTrack = value;
                RaisePropertyChanged("SelectedTrack");
            }
        }

        public string SearchText
        {
            get { return _searchText; }
            set
            {
                if (value == _searchText) return;
                _searchText = value;

                if (string.IsNullOrEmpty(value))
                    TrackViewCollection.Filter = null;
                else
                    TrackViewCollection.Filter = Filtering;

                RaisePropertyChanged(() => SearchText);
            }
        }

        public string NowPlayingSong
        {
            get
            {
                return NowPlaying == null ? string.Empty : string.Format("Сейчас играет: {0}", NowPlaying.TrackName);
            }
        }

        public ShortTrackInfo NowPlaying
        {
            get { return _nowPlaying; }
            set
            {
                if (Equals(value, _nowPlaying)) return;
                _nowPlaying = value;
                RaisePropertyChanged(() => NowPlaying);
                RaisePropertyChanged(() => NowPlayingSong);
            }
        }


        public ObservableCollection<Track> Tracks
        {
            get { return Station.Tracks ?? (Station.Tracks = new ObservableCollection<Track>()); }
        }



        /// <summary>
        /// Order collection
        /// </summary>
        private ListCollectionView _trackView;

        private string _searchText;
        private bool _isSortByName;
        private bool _isSortByDuration;
        private bool _isSortByBitrate;
        private bool _isSortByNull;
        private string _statusMessage;
        private ShortTrackInfo _nowPlaying;

        public ListCollectionView TrackViewCollection
        {
            get { return _trackView; }
            set
            {
                _trackView = value;
                RaisePropertyChanged("TrackViewCollection");
            }
        }

        public int TracksCount
        {
            get
            {
                if (Tracks == null)
                    return 0;

                return Tracks.Count();
            }
        }

        public string CommonDuration
        {
            get
            {
                if (Tracks == null)
                    return "00:00:00";

                return TimeSpan.FromSeconds(Tracks.Sum(x => x.Duration)).ToString();
            }
        }

        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                RaisePropertyChanged("IsBusy");
            }
        }

        public bool IsSaveButtonEnabled
        {
            get { return _isSaveButtonEnabled; }
            set
            {
                _isSaveButtonEnabled = value;
                RaisePropertyChanged("IsSaveButtonEnabled");
            }
        }

        public string StatusMessage
        {
            get { return _statusMessage; }
            set
            {
                if (value == _statusMessage) return;
                _statusMessage = value;
                RaisePropertyChanged(() => StatusMessage);
            }
        }

        #region Sorting

        public bool IsSortByNull
        {
            get { return _isSortByNull; }
            set
            {
                _isSortByNull = value;

                if (value)
                {
                    TrackViewCollection.SortDescriptions.Clear();
                }

                RaisePropertyChanged(() => IsSortByNull);
            }
        }

        public bool IsSortByName
        {
            get { return _isSortByName; }
            set
            {
                _isSortByName = value;

                if (value)
                {
                    TrackViewCollection.SortDescriptions.Clear();
                    TrackViewCollection.SortDescriptions.Add(new SortDescription("Artist", ListSortDirection.Ascending));
                }

                RaisePropertyChanged("IsSortByName");
            }
        }

        public bool IsSortByDuration
        {
            get { return _isSortByDuration; }
            set
            {
                _isSortByDuration = value;

                if (value)
                {
                    TrackViewCollection.SortDescriptions.Clear();
                    TrackViewCollection.SortDescriptions.Add(new SortDescription("Duration", ListSortDirection.Ascending));
                }

                RaisePropertyChanged(() => IsSortByDuration);
            }
        }

        public bool IsSortByBitrate
        {
            get { return _isSortByBitrate; }
            set
            {
                TrackViewCollection.SortDescriptions.Clear();
                TrackViewCollection.SortDescriptions.Add(new SortDescription("Bitrate", ListSortDirection.Ascending));

                RaisePropertyChanged(() => IsSortByBitrate);
            }
        }

        #endregion

        #endregion

    }
}
