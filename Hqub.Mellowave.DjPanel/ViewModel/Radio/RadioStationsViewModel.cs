﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using Hqub.Mellowave.DjPanel.DTO;
using Hqub.Mellowave.DjPanel.Utilities;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.ServiceLocation;

namespace Hqub.Mellowave.DjPanel.ViewModel.Radio
{
    public class RadioStationsViewModel : BaseViewModel
    {
        #region Fields

        private ObservableCollection<RadioStation> _stations;
        private RadioStation _selectedStation;
        private MediaPlayerWrapper _mediaPlayerWrapper;

        private Track _lastStationTrack;
        private RadioStation _lastStationPlay;

        #endregion

        #region .ctor

        public RadioStationsViewModel()
        {
            _mediaPlayerWrapper = new MediaPlayerWrapper();
        }

        #endregion

        #region Commands

        public ICommand OpenRadioTabCommand
        {
            get { return new DelegateCommand(OpenRadioTabCommandExecute); }
        }

        private void OpenRadioTabCommandExecute()
        {
            if (SelectedStation == null || TabControlHelper.Exists(SelectedStation.Name.Eng))
                return;

            var controlContent = new ContentControl();

            var regionManager = ServiceLocator.Current.GetInstance<IRegionManager>();

            RegionManager.SetRegionManager(controlContent, regionManager);
            RegionManager.SetRegionName(controlContent, RegionNames.RadioPanelControl+SelectedStation.Name);

            var tab = new TabItem
                          {
                              Content = controlContent,
                              Header = SelectedStation.Name,
                              Tag = SelectedStation.StationId,
                              IsSelected = true,
                              Style = (Style)App.Current.FindResource("TabItemWithCloseButtonTemplate")
                          };

            // Загруажем модуль со станцией
            var navigation = ServiceLocator.Current.GetInstance<Utilities.Navigation>();
            navigation.Go(new Modules.RadioStationModule(SelectedStation), RegionNames.RadioPanelControl+SelectedStation.Name);

            TabControlHelper.Add(SelectedStation.Name.Eng, tab);
        }

        public ICommand RefreshCommand { get {return new DelegateCommand(RefreshCommandExecute);} }

        private void RefreshCommandExecute()
        {
            LoadStations();
        }

        public ICommand PlayStopCommand
        {
            get { return new DelegateCommand<RadioStation>(PlayStopCommandExecute); }
        }

        private void PlayStopCommandExecute(RadioStation radioStation)
        {
            // Если ни одна станция не была еще запущена:
            if(_lastStationTrack == null)
            {
                _lastStationTrack = new Track
                                        {
                                            VkUrl = radioStation.RadioUrl
                                        };

                _mediaPlayerWrapper.Play(_lastStationTrack);
                radioStation.IsPlays = true;
                _lastStationPlay = radioStation;

                return;
            }

            //Иначе останавливаем воспроизведение
            _mediaPlayerWrapper.Stop(false);
            _lastStationPlay.IsPlays = false;

            //
            //Теперь либо станцию просто выключили, либо нажали плей у другой
            //

            // Нажата кнопка Стоп у текущей проигрываемой станции
            if(_lastStationPlay == radioStation)
            {
                _lastStationTrack = null;
                return;
            }

            // Нажата кнопка Играть у другой станции
            _lastStationPlay = radioStation;
            _lastStationPlay.IsPlays = true;
            _lastStationTrack = new Track
                                    {
                                        VkUrl = radioStation.Url
                                    };

            // Запускаем радио ;)
            _mediaPlayerWrapper.Play(_lastStationTrack);

        }

        #endregion

        #region Methods

        protected override void LoadCommandExecute(RoutedEventArgs args)
        {
            base.LoadCommandExecute(args);

            GlobalCommands.RefreshCommand.RegisterCommand(RefreshCommand);

            LoadStations();
        }

        private async void LoadStations()
        {
            try
            {
                var service = ServiceLocator.Current.GetInstance<ServiceClient>();
                var stations = await service.GetStationsAsync();

                Stations = new ObservableCollection<RadioStation>(stations);
            }
            catch (Exception exception)
            {
                Logger.Main.ErrorException(string.Format("LoadStations. {0}", exception.Message), exception);
            }
            finally
            {
                IsLoadRadioStationBusy = false;
            }
        }

        private bool Filtering(object o)
        {
            var station = o as RadioStation;

            if (station == null)
                return false;

            if (string.IsNullOrEmpty(SearchStationName))
                return false;

            var lowerStationName = SearchStationName.ToLower();

            return station.Name.Eng.ToLower().Contains(lowerStationName) ||
                   station.Name.Rus.ToLower().Contains(lowerStationName) ||
                   station.GroupId.ToLower().Contains(lowerStationName);
        }

        #endregion

        #region Binding Properties

        public ObservableCollection<RadioStation> Stations
        {
            get { return _stations; }
            set
            {
                _stations = value;

                StationCollectionView = new ListCollectionView(Stations);
                StationCollectionView.SortDescriptions.Add(new SortDescription("GroupId", ListSortDirection.Ascending));

                RaisePropertyChanged("Stations");
            }
        }

        public ListCollectionView StationCollectionView
        {
            get { return _stationCollectionView; }
            set
            {
                if (Equals(value, _stationCollectionView)) return;
                _stationCollectionView = value;
                RaisePropertyChanged(() => StationCollectionView);
            }
        }

        public RadioStation SelectedStation
        {
            get { return _selectedStation; }
            set
            {
                _selectedStation = value;
                RaisePropertyChanged("SelectedStation");
            }
        }

        private TabItem _selectedTab;
        private bool _isLoadRadioStationBusy;
        private ListCollectionView _stationCollectionView;
        private string _searchStationName;

        public TabItem SelectedTab
        {
            get { return _selectedTab; }

            set
            {
                _selectedTab = value;
                RaisePropertyChanged("SelectedTab");
            }
        }

        public string SearchStationName
        {
            get { return _searchStationName; }
            set
            {
                
                _searchStationName = value;

                if (string.IsNullOrEmpty(value))
                    StationCollectionView.Filter = null;
                else
                {
                    StationCollectionView.Filter = Filtering;
                }

                RaisePropertyChanged(() => SearchStationName);
            }
        }

        public bool IsLoadRadioStationBusy
        {
            get { return _isLoadRadioStationBusy; }

            set
            {
                _isLoadRadioStationBusy = value;
                RaisePropertyChanged("IsLoadRadioStationBusy");
            }
        }

        #endregion
    }
}
