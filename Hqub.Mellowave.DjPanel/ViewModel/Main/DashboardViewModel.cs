﻿using System.Collections.ObjectModel;
using System.Windows.Input;
using Hqub.Mellowave.DjPanel.DTO;
using Hqub.Mellowave.DjPanel.Model;
using Hqub.Mellowave.DjPanel.ViewModel.Dialogs;
using Hqub.Mellowave.DjPanel.ViewModel.Radio;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.ServiceLocation;

namespace Hqub.Mellowave.DjPanel.ViewModel.Main
{
    public class DashboardViewModel : BaseViewModel
    {
        protected override void LoadCommandExecute(System.Windows.RoutedEventArgs args)
        {
            base.LoadCommandExecute(args);

            RadioStationsShowCommand.Execute(null);
        }

        #region Commands

        public ICommand RadioStationCreateCommand
        {
            get { return new DelegateCommand(RadioStationCreateCommandExecute); }
        }

        private void RadioStationCreateCommandExecute()
        {
            var station = new RadioStation
                {
                    Oid = new PythonObjectId(MongoDB.Bson.ObjectId.GenerateNewId()),
                    Name = new LanguageTranslator(),
                    Tracks = new ObservableCollection<Track>(),
                    IsCreated = true,
                    StationType = "expert"
                };

            var stationDialog = new View.Radio.RadioSettingsDialogView(new RadioSettingsDialogViewModel(station));

            stationDialog.ShowDialog();

            UpdateStationsCommandExecute();
        }

        public ICommand TrackSearchCommand{get{return new DelegateCommand(TrackSearchCommandExecute);}}

        private void TrackSearchCommandExecute()
        {
            var dialog = new View.Dialogs.SearchAudioDialogView(new SearchAudioDialogViewModel(true));
            dialog.ShowDialog();
        }

        public ICommand RadioStationsShowCommand
        {
            get { return new DelegateCommand(RadioStationsShowCommandExecute); }
        }

        private void RadioStationsShowCommandExecute()
        {
            var navigation = ServiceLocator.Current.GetInstance<Navigation.NavigationManager>();

            var radioModuleType = typeof (Modules.RadioModule);

            if (navigation.ExitsModule(radioModuleType))
                navigation.GoTo(radioModuleType);
            else
                navigation.GoNext(ManagerModule.LoadModule(radioModuleType));
        }


        public ICommand UpdateStationsCommand
        {
            get { return new DelegateCommand(UpdateStationsCommandExecute); }
        }

        private void UpdateStationsCommandExecute()
        {
            GlobalCommands.RefreshCommand.Execute(null);
        }
        #endregion
    }
}
