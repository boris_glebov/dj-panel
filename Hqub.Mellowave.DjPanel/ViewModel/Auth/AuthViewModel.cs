﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Navigation;
using Hqub.Mellowave.DjPanel.Model;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.ServiceLocation;

namespace Hqub.Mellowave.DjPanel.ViewModel.Auth
{
    public class AuthViewModel : BaseViewModel
    {
        #region Fields

        #endregion

        #region .ctor

        public AuthViewModel()
        {

        }

        #endregion

        #region Commands

        public ICommand RedirectCommand { get { return new DelegateCommand<NavigationEventArgs>(RedirectCommandExecute); } }

        private void RedirectCommandExecute(NavigationEventArgs eventArgs)
        {
            Logger.Main.Debug(string.Format("Redirect to {0}", eventArgs.Uri));

            var args = ParseResult(eventArgs.Uri);

            if (!args.ContainsKey("access_token") && !args.ContainsKey("user_id") && NavigateCommand != null)
            {
                return;
            }
            
            //Создаем пользователя в кэше и загружаем главный модуль системы:
            Cache.Instance.SetIdentity(new User(args["user_id"], args["access_token"]));

            //Проходим аутентификацию на сервисе API:
            var authResponse = Auth();
            if (authResponse == null)
            {
                var error = "Авторизация не пройдена. Обратитесь к администратору.";
                System.Windows.MessageBox.Show(error);
                Logger.Main.Error(error);

                return;
            }

            if (authResponse.IsError)
            {
                Logger.Main.Error(authResponse.Error);
                System.Windows.MessageBox.Show(string.Format("Server Error: {0}", authResponse.Error));

                return;
                
            }

            ManagerModule.LoadModule(typeof(Modules.MainModule));
        }

        private DTO.Response.AuthResponse Auth()
        {
            try
            {
                var serviceClient = ServiceLocator.Current.GetInstance<ServiceClient>();

                return serviceClient.Auth(Cache.Instance.GetIdentity().UserId);
            }
            catch (Exception exception)
            {
                Logger.Main.ErrorException(
                    string.Format("AuthViewModel.RedirectCommandExecute. {0}", exception.Message), exception);

                return null;
            }
        }

        private Action<string> NavigateCommand { get; set; } 

        #endregion

        #region Methods

        private Dictionary<string, string> ParseResult(Uri url)
        {
            var dictionary = new Dictionary<string, string>();

            if (string.IsNullOrEmpty(url.Fragment))
                return dictionary;

            var args = url.Fragment.Split('&');


            var firstElement = true;
            foreach (var arg in args)
            {
                var s = arg.Split('=');

                if (s.Length < 2)
                {
                    Logger.Main.Warn(
                        string.Format(
                            "Метод ParseResult. При разборе строки с параметрами, попался параметр без значения.\n{0}", url));

                    continue;
                }

                //Первый элемент приходит с первым символом: '#'. Удалим его.
                if(firstElement && s[0][0] == '#')
                {
                    firstElement = false;
                    s[0] = s[0].Substring(1);
                }

                dictionary.Add(s[0], s[1]);
            }

            return dictionary;
        }

        public void SetNavigateCommand(Action<string> navigateCommand)
        {
            NavigateCommand = navigateCommand;
        }

        #endregion

        #region Binding Properties

        public string Source
        {
            get
            {
                var url = string.Format("{0}?client_id={1}&scope={2}&redirect_uri={3}&display={4}&response_type={5}",
                                         Properties.Settings.Default.AuthUrl,
                                         Properties.Settings.Default.ClientId,
                                         Properties.Settings.Default.Scope,
                                         Properties.Settings.Default.RedirectUri,
                                         Properties.Settings.Default.Display,
                                         Properties.Settings.Default.ResponseType
                    );

                return url;
            }
        }

        #endregion
    }
}
