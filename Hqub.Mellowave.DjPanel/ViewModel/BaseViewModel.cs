﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using Hqub.Mellowave.DjPanel.DTO;
using Hqub.Mellowave.DjPanel.Utilities;
using Microsoft.Practices.Prism.Commands;

namespace Hqub.Mellowave.DjPanel.ViewModel
{
    public class BaseViewModel : Microsoft.Practices.Prism.ViewModel.NotificationObject
    {
        protected MediaPlayerWrapper _mediaPlayerWrapper;

        public ICommand LoadCommand
        {
            get { return new DelegateCommand<RoutedEventArgs>(LoadCommandExecute); }
        }

        protected virtual void LoadCommandExecute(RoutedEventArgs args)
        {

        }

        public ICommand ClosingCommand { get { return new DelegateCommand<CancelEventArgs>(ClosingCommandExecute); } }

        protected virtual void ClosingCommandExecute(CancelEventArgs args)
        {
            
        }

        #region Playr Commands

        public ICommand PositionChangedCommand
        {
            get { return new DelegateCommand<RoutedPropertyChangedEventArgs<Double>>(PositionChangedCommandExecute); }
        }

        private void PositionChangedCommandExecute(RoutedPropertyChangedEventArgs<Double> positionChangedEventArgs)
        {
            _mediaPlayerWrapper.Rewind(positionChangedEventArgs.NewValue);
        }

        public ICommand PositionMouseEnterCommand
        {
            get { return new DelegateCommand<Track>(MouseEnterCommandExecute); }
        }

        private void MouseEnterCommandExecute(Track track)
        {
            if (track == null)
                return;

            if (track.IsPlays)
                track.SliderVisibility = 1;
        }


        public ICommand PositionMouseLeaveCommand
        {
            get { return new DelegateCommand<Track>(MouseLeaveCommandExecute); }
        }

        private void MouseLeaveCommandExecute(Track track)
        {
            if (track == null)
                return;

            track.SliderVisibility = 0;
        }

        #endregion
    }
}
