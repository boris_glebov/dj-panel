﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Hqub.Mellowave.DjPanel.DTO;
using Hqub.Mellowave.DjPanel.Utilities;
using Microsoft.Practices.Prism.Commands;

namespace Hqub.Mellowave.DjPanel.ViewModel.Dialogs
{
    public class GenerateFishByGenresViewModel : BaseDialogViewModel
    {
        private Genre _selectedGenre;
        private ObservableCollection<Genre> _choosenGenres;
        private ObservableCollection<Genre> _genres;
        private ObservableCollection<Genre> _searchedGenres;

        public GenerateFishByGenresViewModel()
        {
            Genres = new ObservableCollection<Genre>(GenresStoreUtils.GenresStore.Genres);
            SearchedGenres = new ObservableCollection<Genre>();
            ChoosenGenres = new ObservableCollection<Genre>();
        }

        #region Commands

        public ICommand AddGenreToListCommand
        {
            get { return new DelegateCommand(AddGenreToListCommandExecute); }
        }

        private void AddGenreToListCommandExecute()
        {
            foreach (
                var searchedGenre in
                    SearchedGenres.Where(searchedGenre => ChoosenGenres.All(x => x.Name != searchedGenre.Name)))
            {
                ChoosenGenres.Add(searchedGenre);
            }

            SearchedGenres.Clear();
        }


        public ICommand GenerateCommand
        {
            get { return new DelegateCommand(GenerateCommandExecute); }
        }

        private void GenerateCommandExecute()
        {
            GeneratedTracks = new List<Track>();

            if (ChoosenGenres.Count == 0)
            {
                MessageBox.Show("Список жанров пуст.");
                return;
            }

            foreach (var choosenGenre in ChoosenGenres)
            {
                try
                {
                    GeneratedTracks.AddRange(EchonestHelper.GetTracsksByGenre(100, choosenGenre.Name));
                }
                catch(Exception ex)
                {
                    //есть вероятность проблемы с доступом к echonest (обычно временные)

                    Logger.Main.ErrorException(string.Format("GenerateCommandExecute error. {0}", ex.Message), ex);
                }
            }

            CloseDialog();
        }


        public ICommand CancelCommand
        {
            get { return new DelegateCommand(CancelCommandExecute); }
        }

        private void CancelCommandExecute()
        {
            
        }


        public ICommand RemoveCommand
        {
            get { return new DelegateCommand(RemoveCommandExecute); }
        }

        private void RemoveCommandExecute()
        {
            if(SelectedGenre == null)
                return;

            ChoosenGenres.Remove(SelectedGenre);
        }


        public ICommand ClearCommand
        {
            get { return new DelegateCommand(ClearCommandExecute); }
        }

        private void ClearCommandExecute()
        {
            ChoosenGenres.Clear();
        }


        public ICommand SearchByEnterPressCommand
        {
            get { return new DelegateCommand<KeyEventArgs>(SearchByEnterPressCommandExecute); }
        }

        private void SearchByEnterPressCommandExecute(KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                AddGenreToListCommandExecute();
            }
        }

        #endregion

        #region Binding Properties

        public Genre SelectedGenre
        {
            get { return _selectedGenre; }
            set
            {
                _selectedGenre = value;
                RaisePropertyChanged(() => SelectedGenre);
            }
        }

        public ObservableCollection<Genre> SearchedGenres
        {
            get { return _searchedGenres; }
            set
            {
                _searchedGenres = value;
                RaisePropertyChanged(() => SearchedGenres);
            }
        }

        public ObservableCollection<Genre> Genres
        {
            get { return _genres; }
            set
            {
                _genres = value;
                RaisePropertyChanged(() => Genres);
            }
        }

        public ObservableCollection<Genre> ChoosenGenres
        {
            get { return _choosenGenres; }
            set
            {
                _choosenGenres = value;
                RaisePropertyChanged(() => ChoosenGenres);
            }
        }

        public List<Track> GeneratedTracks { get; set; } 

        #endregion
    }
}
