﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Hqub.Mellowave.DjPanel.DTO;
using Hqub.Mellowave.DjPanel.Utilities;
using Hqub.Mellowave.Vkontakte.API;
using Microsoft.Practices.Prism.Commands;

namespace Hqub.Mellowave.DjPanel.ViewModel.Dialogs
{
    public class SearchAudioDialogViewModel : BaseDialogViewModel
    {
        #region Fields

        private string _searchText;
        private bool _isBusy;
        private ObservableCollection<Track> _tracks;
        private ObservableCollection<Track> _choosenTracks;
        private bool _isDownloader;

        
        private Track _choosenTrackSelected;
        private Track _trackSelected;

        private int _offset = 0;
        private bool _isRightBusy;
        private bool _isOpenFolderAfterDownload;
        private bool _acceptButtonEnable;
        private string _statusInfo;

        #endregion

        #region .ctor

        public SearchAudioDialogViewModel(bool isDownloader=false)
        {
            _mediaPlayerWrapper = new MediaPlayerWrapper();
            _mediaPlayerWrapper.MediaEnd += OnPlayTrackEnded;

            IsDownloader = isDownloader;

            ChoosenTracks = new ObservableCollection<Track>();

            IsOpenFolderAfterDownload = true;
            AcceptButtonEnable = true;
        }

        #endregion

        #region Commands

        /// <summary>
        /// Поиск трэков в ВК
        /// </summary>
        public ICommand SearchCommand
        {
            get { return new DelegateCommand(SearchCommandExecute); }
        }

        private async void SearchCommandExecute()
        {
            await SearchAsync();
            await SetBitrateAsync();
        }

  
        /// <summary>
        /// Сообщает о том, что все записи выбраны
        /// </summary>
        public ICommand AcceptCommand
        {
            get { return new DelegateCommand(AcceptCommandExecute); }
        }

        private async void AcceptCommandExecute()
        {
            IsRightBusy = true;
            AcceptButtonEnable = false;

            await AcceptCommandAsync();
        }

        /// <summary>
        /// Обработка accept button для выкачивания музыки из ВК
        /// </summary>
        private void DownloaderAcceptCommand()
        {
            var basePath = string.Format("{0}Music\\", AppDomain.CurrentDomain.BaseDirectory);

            try
            {
                if (ChoosenTracks.Count == 0)
                    return;

                //создадим папку Music
                if (!System.IO.Directory.Exists(basePath))
                {
                    System.IO.Directory.CreateDirectory(basePath);
                }

                var webClient = new WebClient();

                foreach (var choosenTrack in ChoosenTracks.Where(x => !string.IsNullOrEmpty(x.VkUrl)))
                {
                    StatusInfo = string.Format("Идет загрузка \"{0}\".", choosenTrack.FullName);

                    var file = webClient.DownloadData(choosenTrack.VkUrl);
                    if (file == null)
                        continue;

                    var fileName = string.Format("{0}{1}.mp3", basePath, choosenTrack.FullName);

                    try
                    {

                        System.IO.File.WriteAllBytes(fileName, file);
                        
                        choosenTrack.Hash = Hqub.Mellowave.Vkontakte.API.Security.Md5Helper.GetMd5Hash(MD5.Create(),
                                                                                        System.Text.Encoding.UTF8
                                                                                              .GetString(
                                                                                                  file));
                    }
                    catch (Exception ex)
                    {
                        var message = string.Format("Не удалось сохранить трэк '{0}' на диск.", fileName);
                        MessageBox.Show(message);

                        Logger.Main.ErrorException(string.Format("{0}. {1}", message, ex.Message), ex);
                    }
                }
            }
            catch (Exception exception)
            {
                Logger.Main.ErrorException(string.Format("DownloaderAcceptCommand Exception. {0}", exception.Message),
                                           exception);
            }
            finally
            {
                AcceptButtonEnable = true;
                IsRightBusy = false;
                StatusInfo = "Обработка завершена.";

                if (IsOpenFolderAfterDownload)
                    System.Diagnostics.Process.Start("explorer.exe", basePath);
            }
        }

        /// <summary>
        /// Обработка accept button  в режиме поиска музыки для плейлиста
        /// </summary>
        private void FinderAcceptCommand()
        {
            Application.Current.Dispatcher.Invoke(CloseDialog);
        }

        private Task AcceptCommandAsync()
        {
            var task = IsDownloader ? new Task(DownloaderAcceptCommand) : new Task(FinderAcceptCommand);
            task.Start();

            return task;
        }

        public ICommand PlayStopCommand
        {
            get { return new DelegateCommand<Track>(PlayStopCommandExecute); }
        }

        private void PlayStopCommandExecute(Track track)
        {
            try
            {
                if (track == null)
                    return;

                 _mediaPlayerWrapper.Play(track);        
            }
            catch (Exception exception)
            {
                Logger.Main.ErrorException(
                    string.Format("PlayStopCommandExecute. Id:{0} Url: {1}", track.VkId, track.VkUrl), exception);
            }
        }

        /// <summary>
        /// Закрываем диалог
        /// </summary>
        public ICommand CloseCommand
        {
            get { return new DelegateCommand(CloseCommandExecute); }
        }

        private void CloseCommandExecute()
        {
            ChoosenTracks = null;

            CloseDialog();
        }

        protected override void ClosingCommandExecute(CancelEventArgs args)
        {
            _mediaPlayerWrapper.Stop(false);
        }

        public ICommand SearchByEnterPressCommand
        {
            get { return new DelegateCommand<KeyEventArgs>(SearchByEnterPressCommandExecute); }
        }

        private void SearchByEnterPressCommandExecute(KeyEventArgs args)
        {
            switch (args.Key)
            {
                case Key.Enter:
                    SearchCommand.Execute(null);
                    args.Handled = true;
                    break;
                case Key.Escape:
                    SearchText = string.Empty;
                    args.Handled = true;
                    break;
            }
        }

        //Следующая страница
        public ICommand NextPageCommand
        {
            get { return new DelegateCommand(NextPageCommandExecute); }
        }

        private async void NextPageCommandExecute()
        {
            IsBusy = true;
            await SearchAsync(_offset + Properties.Settings.Default.AudioSearchCount);
            await SetBitrateAsync();

        }

        #region Команды контекстного меню

        public ICommand PlaySelectedTrackCommand
        {
            get { return new DelegateCommand(PlaySelectedTrackCommandExecute); }
        }

        private void PlaySelectedTrackCommandExecute()
        {
            if (TrackSelected != null)
                _mediaPlayerWrapper.Play(TrackSelected);
        }

        public ICommand PlaySelectedChoosenTrackCommand
        {
            get { return new DelegateCommand(PlaySelectedChoosenTrackCommandExecute); }
        }

        private void PlaySelectedChoosenTrackCommandExecute()
        {
            if (ChoosenTrackSelected != null)
                _mediaPlayerWrapper.Play(ChoosenTrackSelected);
        }

        public ICommand ChoosenTrackCommand
        {
            get { return new DelegateCommand(ChoosenTrackCommandExecute); }
        }

        private void ChoosenTrackCommandExecute()
        {
          if(TrackSelected == null)
              return;

            ChoosenTrack(TrackSelected);

            TrackSelected = null;
        }

        public ICommand RemoveChoosenTrackCommand
        {
            get{return new DelegateCommand(RemoveChoosenTrackCommandExecute);}
        }

        private void RemoveChoosenTrackCommandExecute()
        {
            if(ChoosenTrackSelected == null)
                return;

            ChoosenTracks.Remove(ChoosenTrackSelected);

            ChoosenTrackSelected = null;
        }

        public ICommand ClearChoosenListCommand {get{return new DelegateCommand(ClearChoosenListCommandExecute);}}

        private void ClearChoosenListCommandExecute()
        {
            ChoosenTrackSelected = null;
            ChoosenTracks.Clear();
        }

        #endregion

        #endregion

        #region Methods

        private Task SearchAsync(int offset = 0)
        {
            IsBusy = true;

            var task = new Task(() => Search(offset));
            task.Start();

            return task;
        }

        private void Search(int offset = 0)
        {
            try
            {
                _offset = offset;

                //Если запрос пуст, то ничего не делаем:
                if (string.IsNullOrEmpty(SearchText))
                    return;

                //Останавливаем проигрывание:
                _mediaPlayerWrapper.Stop(false);

                var vkApi = new API(Cache.Instance.GetIdentity().AccessToken);

                var result = vkApi.SearchAudio(SearchText, Properties.Settings.Default.AudioSearchCount, offset).Tracks.Select(x => new Track
                    {
                        Artist = x.Artist,
                        Title = x.Title,
                        Duration = x.Duration,
                        VkId = string.Format("{0}_{1}", x.OwnerId, x.Id),
                        VkUrl = x.Url,
                        
                    });


                if (offset != 0)
                {
                    //Добавляем предыдущие результаты в конец списка:
                    var listResult = result.ToList();
                    listResult.AddRange(Tracks);
                    //Обновляем коллекцию:
                    Tracks = new ObservableCollection<Track>(listResult);
                }
                else
                {
                    Tracks = new ObservableCollection<Track>(result);
                }
            }
            catch (Exception exception)
            {
                Logger.Main.ErrorException(string.Format("SearchCommandExecute. Query: {0}.", SearchText), exception);
            }
            finally
            {
                IsBusy = false;
            }
        }


        private void SetBitrate()
        {
            foreach (var track in Tracks.ToArray())
            {
                track.Bitrate = MediaPlayerWrapper.GetBitrate(track.VkUrl, track.Duration);
            }
        }

        private Task SetBitrateAsync()
        {
            var task = new Task(SetBitrate);

            task.Start();

            return task;
        }

        private void ChoosenTrack(Track track)
        {
            // Если этот трэк сейчас проигрывался, то останавилваем его
            // fix бага со длительностью.
            if (_mediaPlayerWrapper.IsCurrent(track))
            {
                _mediaPlayerWrapper.Stop(false);
            }

            Tracks.Remove(track);
            ChoosenTracks.Add(track);
        }

        private void OnPlayTrackEnded(Track track)
        {
            var idx = Tracks.IndexOf(track);

            if (idx == -1)
            {
                var choosenIdx = ChoosenTracks.IndexOf(track);
                if (choosenIdx == -1)
                    return;
                
                NextTrackPlay(choosenIdx + 1, ChoosenTracks);
            }
            else

                NextTrackPlay(idx + 1, Tracks);
        }

        private void NextTrackPlay(int idx, IList<Track> tracks)
        {
            if (idx > tracks.Count - 1)
                return;

              PlayStopCommandExecute(tracks[idx]);
        }

        #endregion


        #region Property Bindings

        public string SearchText
        {
            get { return _searchText; }

            set
            {
                _searchText = value;

                RaisePropertyChanged("SearchText");
            }
        }

        public bool IsOpenFolderAfterDownload
        {
            get { return _isOpenFolderAfterDownload; }
            set
            {
                if (value.Equals(_isOpenFolderAfterDownload)) return;
                _isOpenFolderAfterDownload = value;
                RaisePropertyChanged(() => IsOpenFolderAfterDownload);
            }
        }

        public bool AcceptButtonEnable
        {
            get { return _acceptButtonEnable; }
            set
            {
                if (value.Equals(_acceptButtonEnable)) return;
                _acceptButtonEnable = value;
                RaisePropertyChanged(() => AcceptButtonEnable);
            }
        }

        public bool IsDownloader
        {
            get { return _isDownloader; }
            set
            {
                if (value.Equals(_isDownloader)) return;
                _isDownloader = value;
                RaisePropertyChanged(() => IsDownloader);
                RaisePropertyChanged(() => AcceptCommandTitle);
            }
        }

        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                RaisePropertyChanged("IsBusy");
            }
        }

        public bool IsRightBusy
        {
            get { return _isRightBusy; }
            set
            {
                if (value.Equals(_isRightBusy)) return;
                _isRightBusy = value;
                RaisePropertyChanged(() => IsRightBusy);
            }
        }

        public string AcceptCommandTitle
        {
            get { return IsDownloader ? "Скачать" : "Добавить"; }
            
        }

        public ObservableCollection<Track> Tracks
        {
            get { return _tracks; }

            set
            {
                _tracks = value;
                RaisePropertyChanged("Tracks");
            }
        }

        public Track TrackSelected
        {
            get { return _trackSelected; }
            set
            {
                _trackSelected = value;
                RaisePropertyChanged("TrackSelected");
            }
        }

        public ObservableCollection<Track> ChoosenTracks
        {
            get { return _choosenTracks; }
            set
            {
                _choosenTracks = value;
                RaisePropertyChanged("ChoosenTracks");
            }
        }

        public Track ChoosenTrackSelected
        {
            get { return _choosenTrackSelected; }
            set
            {
                _choosenTrackSelected = value;
                RaisePropertyChanged("ChoosenTrackSelected");
            }
        }

        public string StatusInfo
        {
            get { return _statusInfo; }
            set
            {
                if (value == _statusInfo) return;
                _statusInfo = value;
                RaisePropertyChanged(() => StatusInfo);
            }
        }

        #endregion


    }
}
