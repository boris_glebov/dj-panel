﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Hqub.Mellowave.DjPanel.DTO;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.ServiceLocation;

namespace Hqub.Mellowave.DjPanel.ViewModel.Dialogs
{
    public class StationGroupListViewModel : BaseDialogViewModel
    {
        private ObservableCollection<Group> _groups;
        private bool _isBusy;
        private Group _selectedGroup;
        private bool _isOkStatus = false;

        public StationGroupListViewModel()
        {
            IsBusy = true;
            LoadStationGroups();
        }

        private async void LoadStationGroups()
        {
            try
            {
                var serviceClient = ServiceLocator.Current.GetInstance<ServiceClient>();

                var groups = await serviceClient.GetGroupsAsync();

                Groups = new ObservableCollection<Group>(groups);

            }
            catch (Exception exception)
            {
                Logger.Main.ErrorException(string.Format("LoadStationGroups error. {0}", exception.Message), exception);


            }
            finally
            {
                IsBusy = false;
            }
        }

        #region Commands

        public ICommand CloseCommand
        {
            get{return new DelegateCommand(CloseCommandExecute);}
        }

        private void CloseCommandExecute()
        {
            CloseDialog();
        }

        public ICommand WindowHandleClosedCommand
        {
            get { return new DelegateCommand(WindowHandleClosedCommandExecute); }
        }

        private void WindowHandleClosedCommandExecute()
        {
            if (!_isOkStatus)
                SelectedGroup = null;
        }

        public ICommand AcceptCommand
        {
            get { return new DelegateCommand(AcceptCommandExecute); }
        }

        private void AcceptCommandExecute()
        {
            if (SelectedGroup == null)
            {
                MessageBox.Show("Выберите группу.", "Ошибка.");
                return;
            }

            _isOkStatus = true;
            CloseDialog();
        }

        #endregion

        #region Binding Properties

        public ObservableCollection<Group> Groups
        {
            get { return _groups; }
            set
            {
                if (Equals(value, _groups)) return;
                _groups = value;
                RaisePropertyChanged(() => Groups);
            }
        }

        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                if (value.Equals(_isBusy)) return;
                _isBusy = value;
                RaisePropertyChanged(() => IsBusy);
            }
        }

        public Group SelectedGroup
        {
            get { return _selectedGroup; }
            set
            {
                if (Equals(value, _selectedGroup)) return;
                _selectedGroup = value;
                RaisePropertyChanged(() => SelectedGroup);
            }
        }

        #endregion
    }
}
