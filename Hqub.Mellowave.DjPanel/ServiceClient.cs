﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Hqub.Mellowave.DjPanel.DTO;
using Hqub.Mellowave.DjPanel.DTO.Response;
using Hqub.Mellowave.DjPanel.Model;
using Hqub.Mellowave.DjPanel.Utilities;
using Newtonsoft.Json;
using User = Hqub.Mellowave.DjPanel.DTO.User;

namespace Hqub.Mellowave.DjPanel
{
    public class ServiceClient
    {
        private readonly CookieAwareWebClient _webClient;
        private object _lockObject = new object();

        public ServiceClient()
        {
            System.Net.ServicePointManager.Expect100Continue = false;

            _webClient = new CookieAwareWebClient();
        }

        #region Auth Operation

        public Task<AuthResponse> AuthAsync(string viewerId)
        {
            var task = new Task<AuthResponse>(() => Auth(viewerId));

            task.Start();

            return task;
        }

        public AuthResponse Auth(string viewerId, string server="")
        {
            if (string.IsNullOrEmpty(server))
                server = Properties.Settings.Default.ApiServer;

            var url =
                string.Format("{0}api/security/desktop_auth/viwer/{1}/access_token/{2}/version/{3}",
                              server, viewerId,
                              Md5Helper.GetMd5Hash(MD5.Create(),
                                                                          string.Format("{0}_{1}", viewerId,
                                                                                        Properties
                                                                                            .Settings
                                                                                            .Default
                                                                                            .SecretSign)),

                              string.Format("{0}.{1}", MetaInfo.Major, MetaInfo.Minor));
            
            var response = SafeDownlownloadString(url);


            var auth = JsonConvert.DeserializeObject<DTO.Response.AuthResponse>(response);

            return auth;
        }

        #endregion

        #region Station Operation

        public ShortTrackInfo GetNowPlayingTrack(RadioStation station)
        {
            var url = string.Format("{0}api/radio/get_current_song/{1}", station.ApiUrl, station.StationId);

            var response = SafeDownlownloadString(url);

            var track = JsonConvert.DeserializeObject<ShortTrackInfo>(response);

            return track;
        }

        public Task<ShortTrackInfo> GetNowPlyaingTrackAsync(RadioStation station)
        {
            var task = new Task<ShortTrackInfo>(() => GetNowPlayingTrack(station));

            task.Start();

            return task;
        }

        public Task<List<RadioStation>> GetStationsAsync()
        {
            var task = new Task<List<RadioStation>>(GetStations);

            task.Start();

            return task;
        }

        public List<RadioStation> GetStations()
        {
            var url = string.Format("{0}rest/station/all", Properties.Settings.Default.ApiServer);

            var response = SafeDownlownloadString(url);

            var stations = JsonConvert.DeserializeObject<List<RadioStation>>(response);

            return stations;
        }

        public StationSaveResponse SaveStation(RadioStation station)
        {
            var url = string.Format("{0}rest/station/{1}", station.ApiUrl, station.StationId);

            var identity = Cache.Instance.GetIdentity();

            var authResponse = Auth(identity.UserId, station.ApiUrl);

            if (authResponse.IsError)
            {
                Logger.Main.Error(string.Format("SaveStation. Auth. {0}", authResponse.Error));
                return new StationSaveResponse
                    {
                        Error = "Access denided.",
                        Code = 550
                    };
            }

            lock (_lockObject)
            {
                var response = _webClient.UploadData(url, "PUT", Encoding.UTF8.GetBytes(station.ToJson()));

                return JsonConvert.DeserializeObject<StationSaveResponse>(Encoding.UTF8.GetString(response));
            }

        }

        public CheckStationResponse CheckStation(RadioStation station)
        {
            var url = string.Format("{0}api/radio/check_station/{1}", station.ApiUrl, station.StationId);

            var identity = Cache.Instance.GetIdentity();

            var authResponse = Auth(identity.UserId, station.ApiUrl);

            if (!authResponse.IsError)
            {
                lock (_lockObject)
                {
                    var response = JsonConvert.DeserializeObject<CheckStationResponse>(SafeDownlownloadString(url));

                    return response;
                }
            }

            return new CheckStationResponse
                       {
                           Error = "Access denided.",
                           Code = 550
                       };
        }

        #endregion

        #region Station Groups

        public List<Group> GetGroups()
        {
            var url = string.Format("{0}rest/group/all", Properties.Settings.Default.ApiServer);

            var response = SafeDownlownloadString(url);

            var groups = JsonConvert.DeserializeObject<List<Group>>(response);

            return groups;
        }

        public Task<List<Group>> GetGroupsAsync()
        {
            var task = new Task<List<Group>>(GetGroups);

            task.Start();

            return task;
        }


        public string StartStationEther(RadioStation station)
        {
            var url = string.Format("{0}api/liquidsoap/start/{1}", station.ApiUrl, station.StationId);

            var response = SafeDownlownloadString(url);

            return response;
        }

        public Task<string> StartStationEtherAsync(RadioStation station)
        {
            var task = new Task<string>(() => StartStationEther(station));

            task.Start();

            return task;
        }


        public string StopStationEther(RadioStation station)
        {
            var url = string.Format("{0}api/liquidsoap/stop/{1}", station.ApiUrl, station.StationId);

            var response = SafeDownlownloadString(url);

            return response;
        }

        public Task<string> StopStationEtherAsync(RadioStation station)
        {
            var task = new Task<string>(() => StopStationEther(station));

            task.Start();

            return task;
        }


        public string StationSkipCurrentTrack(RadioStation station)
        {
            var url = string.Format("{0}api/liquidsoap/skip/{1}", station.ApiUrl, station.StationId);

            var response = SafeDownlownloadString(url);

            return response;
        }

        public Task<string> StationSkipCurrentTrackAsync(RadioStation station)
        {
            var task = new Task<string>(() => StationSkipCurrentTrack(station));

            task.Start();

            return task;
        }


        public string ReloadPlaylist(RadioStation station)
        {
            var url = string.Format("{0}api/liquidsoap/reload/{1}", station.ApiUrl, station.StationId);

            var response = SafeDownlownloadString(url);

            return response;
        }

        public Task<string> RealoadPlaylistAsync(RadioStation station)
        {
            var task = new Task<string>(() => ReloadPlaylist(station));
         
            task.Start();

            return task;
        }

        public string StationUptime(RadioStation station)
        {
            var url = string.Format("{0}api/liquidsoap/uptime/{1}", station.ApiUrl, station.StationId);

            var response = SafeDownlownloadString(url);

            return response;
        }

        public Task<string> StationUptimeAsync(RadioStation station)
        {
            var task = new Task<string>(() => StationUptime(station));

            task.Start();

            return task;
        }


        public string StationStatus(RadioStation station)
        {
            var url = string.Format("{0}api/liquidsoap/status/{1}", station.ApiUrl, station.StationId);

            var response = SafeDownlownloadString(url);

            return response;
        }

        public Task<string> StationStatusAsync(RadioStation station)
        {
            var task = new Task<string>(() => StationStatus(station));

            task.Start();

            return task;
        }

        #endregion

        #region Track Operation

        public UploadTrackResponse UploadTrack(RadioStation station, Track track, byte[] file)
        {
            try
            {
                var url = string.Format("{0}rest/track/{1}/station/{2}", station.ApiUrl, track.Hash, station.StationId);

                var identity = Cache.Instance.GetIdentity();

                var authResponse = Auth(identity.UserId, station.ApiUrl);

                if (authResponse.IsError)
                {
                    Logger.Main.Error(string.Format("UploadTrack. Auth. {0}", authResponse.Error));
                    return new UploadTrackResponse()
                    {
                        Error = "Access denided.",
                        Code = 550
                    };
                }

                var data = new UploadTrackEntity(file) {Artist = track.Artist, Title = track.Title};
                var bytes = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(data));

                System.Diagnostics.Debug.WriteLine("Sending {0} bytes.", bytes.Length);

                byte[] response = null;
                
                lock(_lockObject)
                {
                    response = _webClient.UploadData(url, "POST", bytes);
                }

                var baseResponse = JsonConvert.DeserializeObject<UploadTrackResponse>(Encoding.UTF8.GetString(response));

                if (baseResponse.IsError)
                {
                    Logger.Main.Error(string.Format("UploadTrack. {0}", baseResponse.Error));
                    return baseResponse;
                }

                return baseResponse;
            }
            catch (Exception exception)
            {
                Logger.Main.ErrorException(string.Format("UploadTrack (EXCEPTION)\n{0}", exception.Message), exception);
                return null;
            }
        }

        #endregion

        #region Moderator Operations

        public Task<List<User>> GetModeratorsAsync(RadioStation station)
        {
            var task = new Task<List<User>>(() => GetModerators(station));

            task.Start();

            return task;
        }

        public List<User> GetModerators(RadioStation station)
        {
            var url = string.Format("{0}rest/moderator/station/{1}", Properties.Settings.Default.ApiServer, station.StationId);

            var response = SafeDownlownloadString(url);

            var moderators = JsonConvert.DeserializeObject<List<User>>(response);

            return moderators;
        }

        public Task<BaseResponse> DedicateToModeratorsAsync(string userId, RadioStation station)
        {
            var task = new Task<BaseResponse>(() => DedicateToModerators(userId, station));

            task.Start();

            return task;
        }

        public BaseResponse DedicateToModerators(string userId, RadioStation station)
        {
            if (string.IsNullOrEmpty(userId))
                return new BaseResponse
                           {
                               Code = -1,
                               Error = "ID пользователя не может быть пустым."
                           };


            var url = string.Format("{0}rest/moderator/station/{1}", Properties.Settings.Default.ApiServer, station.StationId);

            var user = new User
                           {
                               Oid = new PythonObjectId(MongoDB.Bson.ObjectId.GenerateNewId()),
                               UserId = userId
                           };

            byte[] response = null;

            lock (_lockObject)
            {
                response = _webClient.UploadData(url, "PUT", Encoding.UTF8.GetBytes(user.ToJson()));
            }

            return JsonConvert.DeserializeObject<BaseResponse>(Encoding.UTF8.GetString(response));

        }

        #endregion

        #region Playlist Operations

        public List<Track> GetPlaylist(RadioStation station)
        {
            var url = string.Format("{0}api/playlist/get/station/{1}", station.ApiUrl, station.StationId);


            var identity = Cache.Instance.GetIdentity();

            var authResponse = Auth(identity.UserId, station.ApiUrl);

            if (!authResponse.IsError)
            {
                lock (_lockObject)
                {

                    var response = SafeDownlownloadString(url);

                    var playlist = JsonConvert.DeserializeObject<List<Track>>(response);

                    return playlist;
                }
            }

            return new List<Track>();
        }

        #endregion

        #region Genre Operations

        public IEnumerable<Genre> GetGenres()
        {
            var url = string.Format("{0}rest/genre/{1}/", Properties.Settings.Default.ApiServer, "all");

            var response = SafeDownlownloadString(url);

            var genres = JsonConvert.DeserializeObject<List<Genre>>(response);

            return genres;
        }

        #endregion

        private string SafeDownlownloadString(string url)
        {
            lock (_lockObject)
            {
                try
                {
                    var result = _webClient.DownloadString(url);
                    return result;
                }
                catch (Exception exception)
                {
                    Logger.Main.ErrorException(
                        string.Format("SafeDownloadString eror. {0}", exception.Message), exception);
                    return string.Empty;
                }
            }
        }
    }
}
