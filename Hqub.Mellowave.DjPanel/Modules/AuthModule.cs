﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.ServiceLocation;

namespace Hqub.Mellowave.DjPanel.Modules
{
    [Module(ModuleName = "AuthModule")]
    public class AuthModule : BaseNavigationModule
    {
        public AuthModule()
        {
            RegisterView(RegionNames.MainRegionName, ServiceLocator.Current.GetInstance<View.Auth.AuthView>());
        }

        public override string Name
        {
            get { return "AuthModule"; }
        }
    }
}
