﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hqub.Mellowave.DjPanel.Modules
{
    public abstract class BaseModule
    {
        Dictionary<string, object> region2View =
            new Dictionary<string, object>();

        public virtual void Show()
        {
            foreach (var kv in region2View)
            {
                Utilities.RegionManagerUtils.AddToRegion(kv.Key, kv.Value);
            }
        }

        public virtual void Hide()
        {
            foreach (var kv in region2View)
            {
                Utilities.RegionManagerUtils.ClearRegion(kv.Key);
            }
        }

        protected void RegisterView(string regionName, object view)
        {
            region2View.Add(regionName, view);
        }
    }
}
