﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hqub.Mellowave.DjPanel.ViewModel.Playlist;
using Microsoft.Practices.ServiceLocation;

namespace Hqub.Mellowave.DjPanel.Modules
{
    public class PlaylistModule : BaseNavigationModule
    {
        public PlaylistModule(DTO.RadioStation station)
        {
            RegisterView(RegionNames.RadioPanelControl + station.Name, new View.Playlist.PlaylistView(new PlaylistViewModel(station)));
        }

        public override string Name
        {
            get { return "PlaylistModule"; }
        }
    }
}
