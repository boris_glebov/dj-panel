﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hqub.Mellowave.DjPanel.ViewModel;
using Microsoft.Practices.ServiceLocation;

namespace Hqub.Mellowave.DjPanel.Modules
{
    public class TrackModule : BaseModule
    {
        public TrackModule(DTO.Track track, DTO.RadioStation station)
        {
            RegisterView(RegionNames.RadioPanelControl + station.Name,
                         new View.Radio.TrackView(new ViewModel.Radio.TrackViewModel(track, station)));
        }
    }
}
