﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.ServiceLocation;

namespace Hqub.Mellowave.DjPanel.Modules
{
    [Module(ModuleName = "RadioModule")]
    public class RadioModule : BaseNavigationModule
    {

        public RadioModule()
        {
            RegisterView(RegionNames.DashboardContentRegionName,
                                     ServiceLocator.Current.GetInstance<View.Radio.RadioStationsView>());
        }

        public override string Name
        {
            get { return "RadioModule"; }
        }
    }
}
