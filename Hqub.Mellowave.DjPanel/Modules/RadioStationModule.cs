﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hqub.Mellowave.DjPanel.ViewModel.Radio;

namespace Hqub.Mellowave.DjPanel.Modules
{
    public class RadioStationModule : BaseModule
    {
        public RadioStationModule(DTO.RadioStation station)
        {
            RegisterView(RegionNames.RadioPanelControl+station.Name, new View.Radio.RadioPanelView(new RadioPanelViewModel(station)));
        }
    }
}
