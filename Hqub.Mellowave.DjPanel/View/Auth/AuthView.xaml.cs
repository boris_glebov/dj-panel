﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Hqub.Mellowave.DjPanel.View.Auth
{
    /// <summary>
    /// Interaction logic for AuthView.xaml
    /// </summary>
    public partial class AuthView : BaseUserControlView
    {
        public AuthView(ViewModel.Auth.AuthViewModel viewModel)
            : base(viewModel)
        {
            InitializeComponent();

            viewModel.SetNavigateCommand(webBrowser.Navigate);
            
        }
    }
}
