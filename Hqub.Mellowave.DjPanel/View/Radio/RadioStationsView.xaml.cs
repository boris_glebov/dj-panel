﻿using System.Windows.Controls;
using Microsoft.Practices.Prism.Commands;
using System.Linq;
using System.Collections;

namespace Hqub.Mellowave.DjPanel.View.Radio
{
    /// <summary>
    /// Interaction logic for RadioStationsView.xaml
    /// </summary>
    public partial class RadioStationsView : BaseUserControlView
    {
        public RadioStationsView(ViewModel.Radio.RadioStationsViewModel radioStationsViewModel)
            : base(radioStationsViewModel)
        {
            InitializeComponent();

            RegisterCommands();
        }

        private void RegisterCommands()
        {
            GlobalCommands.OpenRadioTab = new DelegateCommand<object>(Add);
            GlobalCommands.CloseRadioTab = new DelegateCommand<object>(Close);
        }

        private void Add(object item)
        {
            RadioTabs.Items.Add(item);
        }

        private void Close(object item)
        {
            var tab = RadioTabs.Items.Cast<TabItem>().FirstOrDefault(x => x.Header.ToString() == item.ToString());

            if (tab == null)
                return;

            Utilities.TabControlHelper.Remove(tab.Header.ToString());

            RadioTabs.Items.Remove(tab);

            
        }
    }
}
