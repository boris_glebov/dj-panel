﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Practices.Prism.Commands;

namespace Hqub.Mellowave.DjPanel.View.Radio
{
    /// <summary>
    /// Interaction logic for RadioPanelView.xaml
    /// </summary>
    public partial class RadioPanelView : BaseUserControlView
    {
        public RadioPanelView(ViewModel.Radio.RadioPanelViewModel viewModel)
            : base(viewModel)
        {
            InitializeComponent();

            GlobalCommands.UpdateUI.RegisterCommand(new DelegateCommand(() => Playlist.Focus()));
        }
    }
}
