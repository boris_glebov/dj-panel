﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Hqub.Mellowave.DjPanel.View.Radio
{
    /// <summary>
    /// Interaction logic for RadioSettingsDialogView.xaml
    /// </summary>
    public partial class RadioSettingsDialogView : View.BaseWindowView
    {
        public RadioSettingsDialogView(ViewModel.Radio.RadioSettingsDialogViewModel viewModel)
            : base(viewModel)
        {
            InitializeComponent();
        }
    }
}
