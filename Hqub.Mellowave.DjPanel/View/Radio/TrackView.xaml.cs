﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Hqub.Mellowave.DjPanel.View.Radio
{
    /// <summary>
    /// Interaction logic for TrackView.xaml
    /// </summary>
    public partial class TrackView : View.BaseUserControlView
    {
        public TrackView(ViewModel.BaseViewModel viewModel) :base(viewModel)
        {
            InitializeComponent();

            Loaded += (sender, args) =>
                          {
                              textArtistName.Focus();
                          };
        }
    }
}
