﻿using System.Windows;
using Hqub.Mellowave.DjPanel.View;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.ServiceLocation;

namespace Hqub.Mellowave.DjPanel
{
    /// <summary>
    /// Interaction logic for MainWindowView.xaml
    /// </summary>
    public partial class MainWindowView : BaseWindowView
    {
        public MainWindowView(ViewModel.MainWindowViewModel viewModel) : base(viewModel)
        {
            InitializeComponent();

            Title = string.Format("Панель управления интернет радиом (Deepwave v.{0})", MetaInfo.AssemblyVersion);
        }
    }
}
