﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Hqub.Mellowave.DjPanel.View.Playlist
{
    /// <summary>
    /// Interaction logic for PlaylistView.xaml
    /// </summary>
    public partial class PlaylistView : View.BaseUserControlView
    {
        public PlaylistView(ViewModel.Playlist.PlaylistViewModel viewModel) : base(viewModel)
        {
            InitializeComponent();
        }
    }
}
