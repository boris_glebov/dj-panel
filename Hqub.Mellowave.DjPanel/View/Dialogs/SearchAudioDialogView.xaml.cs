﻿using System.Collections.Generic;
using Hqub.Mellowave.DjPanel.DTO;
using Hqub.Mellowave.DjPanel.ViewModel.Dialogs;

namespace Hqub.Mellowave.DjPanel.View.Dialogs
{
    /// <summary>
    /// Interaction logic for SearchAudioDialogView.xaml
    /// </summary>
    public partial class SearchAudioDialogView : BaseWindowView
    {
        public SearchAudioDialogView(SearchAudioDialogViewModel viewModel)
            : base(viewModel)
        {
            InitializeComponent();
        }

        public IEnumerable<Track> ShowAndGetResults()
        {
            ShowDialog();

            return ((SearchAudioDialogViewModel) ViewModel).ChoosenTracks;
        }
    }
}
