﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Hqub.Mellowave.DjPanel.Annotations;

namespace Hqub.Mellowave.DjPanel.View.Dialogs
{
    /// <summary>
    /// Interaction logic for TrackNameEditDialog.xaml
    /// </summary>
    public partial class TrackNameEditDialog : Window, INotifyPropertyChanged
    {

        #region Fields

        private DTO.Track _track;
        private string _title;
        private string _artist;

        #endregion

        #region .ctor

        public TrackNameEditDialog(DTO.Track track)
        {
            InitializeComponent();

            _track = track;

            Parse(track.FullName);
        }

        #endregion

        #region Methods

        private void Parse(string name)
        {
            var split = name.Split('-');

            Artist = split[0];
            TrackTitle = split[1];
        }

        private void BtnOk_OnClick(object sender, RoutedEventArgs e)
        {
            Save();
        }

        private void TrackNameEditDialog_OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
                Close();
            else if(e.Key == Key.Enter)
                Save();
        }

        private void Save()
        {
            _track.Artist = Artist;
            _track.Title = TrackTitle;

            Close();
        }

        #endregion

        #region Properties

        public string Artist
        {
            get { return _artist; }
            set
            {
                _artist = value;
                OnPropertyChanged();
            }
        }

        public string TrackTitle
        {
            get { return _title; }
            set
            {
                _title = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region Implemented INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
