﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Hqub.Mellowave.DjPanel.DTO;
using Hqub.Mellowave.DjPanel.ViewModel;

namespace Hqub.Mellowave.DjPanel.View.Dialogs
{
    /// <summary>
    /// Interaction logic for GenerateFishByGenres.xaml
    /// </summary>
    public partial class GenerateFishByGenres : View.BaseWindowView
    {
        public GenerateFishByGenres(ViewModel.Dialogs.GenerateFishByGenresViewModel viewModel) : base(viewModel) 
        {
            InitializeComponent();
        }

        public List<Track> GeneratedTracks
        {
            get { return ((DjPanel.ViewModel.Dialogs.GenerateFishByGenresViewModel) ViewModel).GeneratedTracks; }
        }

    }
}
