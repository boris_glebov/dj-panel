﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Input;
using Hqub.Mellowave.DjPanel.Annotations;

namespace Hqub.Mellowave.DjPanel.View.Dialogs
{
    /// <summary>
    /// Interaction logic for AddArtistImageDialogView.xaml
    /// </summary>
    public partial class AddArtistImageDialogView : Window, INotifyPropertyChanged
    {
        private string _artistImageUrl;

        public AddArtistImageDialogView()
        {
            InitializeComponent();

            this.Loaded += (sender, args) => textArtistUrlImage.Focus();
        }


        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
           AcceptCommand();
        }


        private void AcceptCommand()
        {
            DialogResult = true;
            Close();
        }

        public string ArtistImageUrl
        {
            get { return _artistImageUrl; }
            set
            {
                if (value == _artistImageUrl) return;
                _artistImageUrl = value;
                OnPropertyChanged();
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        private void UIElement_OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                AcceptCommand();
            }else if( e.Key == Key.Escape)
                Close();
        }
    }
}
