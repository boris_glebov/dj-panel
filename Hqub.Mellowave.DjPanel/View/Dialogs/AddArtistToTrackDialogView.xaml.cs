﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Hqub.Mellowave.DjPanel.Annotations;
using Hqub.Mellowave.DjPanel.DTO;
using Microsoft.Practices.Prism.Commands;

namespace Hqub.Mellowave.DjPanel.View.Dialogs
{
    /// <summary>
    /// Interaction logic for AddArtistToTrackDialogView.xaml
    /// </summary>
    public partial class AddArtistToTrackDialogView : Window, INotifyPropertyChanged
    {
        private string _artistName;
        private bool? _checkLastFm;
        private string _statusMessage;

        private LastFmApi.LastFm _lastApi;

        public AddArtistToTrackDialogView()
        {
            InitializeComponent();

            this.Loaded += (sender, args) => textArtistName.Focus();
            CheckLastFm = null;

            //Create last.fm api instance:
            var lastConfigure = new Model.LastfmConfigEntity();
            _lastApi = new LastFmApi.LastFm(lastConfigure.ApiKey, lastConfigure.Secret);
        }

        public AddArtistToTrackDialogView(string artistName) : this()
        {
            ArtistName = artistName;

            CheckLastFmCommandExecute();
        }

        #region Commands

        public ICommand CheckLastFmCommand { get {return new DelegateCommand(CheckLastFmCommandExecute);} }

        private void CheckLastFmCommandExecute()
        {
            Cursor = Cursors.Wait;

            InvokeCheckLastFm();

            Cursor = Cursors.Arrow;
        }

        private void InvokeCheckLastFm()
        {
            if (string.IsNullOrEmpty(ArtistName))
            {
                CheckLastFm = false;
                ShowError("Введите имя исполнителя.");

                return;
            }

            var artist = _lastApi.ArtistGetInfo(ArtistName, string.Empty, true);

            if (artist == null)
            {
                CheckLastFm = false;
                ShowError("Повашему запросу ничего не найдено. Проверьте правильность имени исполнителя.");

                return;
            }

            ArtistName = artist.Name;

            CheckLastFm = true;
            ShowSusccess("Исполнитель успешно определен.");
        }

        public ICommand AcceptCommand { get{return new DelegateCommand(AcceptCommandExecute);} }

        private void AcceptCommandExecute()
        {
            Cursor = Cursors.Wait;

            Artist = new Artist();

            var artist = _lastApi.ArtistGetInfo(ArtistName, string.Empty, true);

            Artist.Name = artist.Name;
            Artist.MBId = artist.Mbid;
            Artist.Images = Utilities.ArtistImageSearchHelper.GetAtistImages(Artist.Name);

            Cursor = Cursors.Arrow;


            DialogResult = true;
            Close();
        }

        #endregion

        #region Methods

        private void ShowError(string message)
        {
            textStatusMessage.Foreground = new SolidColorBrush(Colors.Red);

            StatusMessage = message;
        }

        private void ShowSusccess(string message)
        {
            textStatusMessage.Foreground = new SolidColorBrush(Colors.Green);

            StatusMessage = message;
        }


        private void UIElement_OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                AcceptCommandExecute();
            }
            else if (e.Key == Key.Escape)
                Close();
        }

        #endregion

        #region Properties

        public DTO.Artist Artist { get; set; }

        #endregion

        #region Binding Properties

        public string ArtistName
        {
            get { return _artistName; }
            set
            {
                if (value == _artistName) return;
                _artistName = value;
                OnPropertyChanged();
            }
        }

        public bool? CheckLastFm
        {
            get { return _checkLastFm; }
            set
            {
                if (value.Equals(_checkLastFm)) return;
                _checkLastFm = value;

                if(value == null)
                    textArtistName.BorderBrush = new SolidColorBrush(Colors.Black);

                textArtistName.BorderBrush = value == true
                                                 ? new SolidColorBrush(Colors.Green)
                                                 : new SolidColorBrush(Colors.Red); 

                OnPropertyChanged();
            }
        }

        public string StatusMessage
        {
            get { return _statusMessage; }
            set
            {
                if (value == _statusMessage) return;
                _statusMessage = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region INotifyPropertyChanged Implementation

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
