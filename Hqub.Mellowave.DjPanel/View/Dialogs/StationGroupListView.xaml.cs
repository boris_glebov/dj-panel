﻿namespace Hqub.Mellowave.DjPanel.View.Dialogs
{
    /// <summary>
    /// Interaction logic for StationGroupListView.xaml
    /// </summary>
    public partial class StationGroupListView : View.BaseWindowView
    {
        public StationGroupListView(ViewModel.Dialogs.StationGroupListViewModel viewModel)
            : base(viewModel)
        {
            InitializeComponent();
        }


        public string StationId
        {
            get { return string.Empty; }
        }

        public string Group
        {
            get
            {
                var group =
                    ((ViewModel.Dialogs.StationGroupListViewModel) ViewModel).SelectedGroup;

                return group != null ? group.GroupId : string.Empty;
            }
        }
    }
}
