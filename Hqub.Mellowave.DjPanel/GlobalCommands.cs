﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Microsoft.Practices.Prism.Commands;

namespace Hqub.Mellowave.DjPanel
{
    public static class GlobalCommands
    {
        public static ICommand OpenRadioTab;
        public static ICommand CloseRadioTab;

        public static CompositeCommand UpdateUI = new CompositeCommand();
        public static CompositeCommand RefreshCommand = new CompositeCommand();
        public static CompositeCommand GlobalTimerTick = new CompositeCommand();
    }
}
