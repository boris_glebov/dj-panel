﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Threading;

namespace Hqub.Mellowave.DjPanel.Utilities
{
    public class GlobalTimer
    {
        private readonly DispatcherTimer _timer;

        public GlobalTimer()
        {
            _timer = new DispatcherTimer
                         {
                             Interval =
                                 TimeSpan.FromMilliseconds(
                                     Properties.Settings.Default.GlobalTimerInterval)
                         };

            _timer.Tick += _timer_Tick;
            _timer.Start();
        }

        private void _timer_Tick(object sender, EventArgs e)
        {
            try
            {
                GlobalCommands.GlobalTimerTick.Execute(null);
            }
            catch (Exception exception)
            {
                Logger.Main.ErrorException(string.Format("Global timer tick error. {0}", exception.Message), exception);
            }
        }
    }
}
