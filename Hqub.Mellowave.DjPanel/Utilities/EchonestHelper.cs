﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EchoNest;
using EchoNest.Playlist;
using EchoNest.Song;
using Hqub.Mellowave.DjPanel.DTO;

namespace Hqub.Mellowave.DjPanel.Utilities
{
    public static class EchonestHelper
    {
        public static IEnumerable<Track> GetTracsksByGenre(int count, params string[] genres)
        {
            var styleTerms = new TermList();

            foreach (var genre in genres)
            {
                styleTerms.Add(genre);
            }

            var staticArgument = new StaticArgument
            {
                Results = count,
                Adventurousness = 0.4,
                Type = "artist-description",
                Variety = 0.4 /* variety of artists */
            };

            staticArgument.Styles.AddRange(styleTerms);

            using (var session = new EchoNestSession(Properties.Settings.Default.EchonestApiKey))
            {
                //act
                var searchResponse = session.Query<Static>().Execute(staticArgument);

                return searchResponse.Songs.Select(x => new Track
                                                            {
                                                                Artist = x.ArtistName,
                                                                Title = x.Title
                                                            });
            }
        }
    }
}
