﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.ServiceLocation;

namespace Hqub.Mellowave.DjPanel.Utilities
{
    public static class TabControlHelper
    {
        public static Dictionary<string, TabItem> Panels = new Dictionary<string, TabItem>();

        public static bool Exists(string panelName)
        {
            return Panels.ContainsKey(panelName);
        }

        public static void Add(string panelName, TabItem panel)
        {
            if(GlobalCommands.OpenRadioTab == null || Panels.Keys.Contains(panelName))
                return;

            Panels.Add(panelName, panel);

            GlobalCommands.OpenRadioTab.Execute(panel);
        }

        public static void Remove(string panelName)
        {
            if(GlobalCommands.CloseRadioTab == null)
                return;

            if(!Panels.ContainsKey(panelName))
            {
                throw new KeyNotFoundException(string.Format("Ключ '{0}' не найден.", panelName));
            }

            Panels.Remove(panelName);

            var navigation = ServiceLocator.Current.GetInstance<Navigation>();

            navigation.RemoveModule(RegionNames.RadioPanelControl + panelName);
        }
    }
}
