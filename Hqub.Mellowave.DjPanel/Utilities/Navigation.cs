﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hqub.Mellowave.DjPanel.Modules;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.ServiceLocation;

namespace Hqub.Mellowave.DjPanel.Utilities
{
    public class Navigation
    {
        private Dictionary<string, Stack<BaseModule>> _modules;

        public Navigation()
        {
            _modules = new Dictionary<string, Stack<BaseModule>> {{"common", new Stack<BaseModule>()}};
        }

        public void Go(BaseModule module, string name="common")
        {
            Stack<BaseModule> stack;

            if (_modules.ContainsKey(name))
            {
                stack = _modules[name];
            }
            else
            {
                stack = new Stack<BaseModule>();
                _modules.Add(name, stack);
            }

            if (stack.Count > 0)
                stack.Peek().Hide();

            stack.Push(module);

            module.Show();
        }

        public void RemoveModule(string name)
        {
            Stack<BaseModule> stack;

            if (!_modules.ContainsKey(name))
                return;

            stack = _modules[name];

            // Очищаем стэк
            while (stack.Count > 0)
            {
                stack.Pop();
            }

            //Удаляем стэк из коллекцию модулей
            _modules.Remove(name);

            RemoveStack(name);
        }


        public void Back(string name="common")
        {
            if (!_modules.ContainsKey(name)) return;

            var stack = _modules[name];

            if (stack.Count <= 0) return;

            stack.Pop();

            if (stack.Count == 0)
            {
                RemoveStack(name);
                return;
            }

            var currentModule = stack.Peek();
            currentModule.Show();
        }

        private void RemoveStack(string name)
        {
            // Если в стеки больше нет контролов, значит контрол с регионом уничтожен
            // Удаляем запись о регионе из менеджера:
            var regionManager = ServiceLocator.Current.GetInstance<IRegionManager>();
            regionManager.Regions.Remove(name);
        }
    }
}
