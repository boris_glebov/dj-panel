﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.ServiceLocation;

namespace Hqub.Mellowave.DjPanel.Utilities
{
    public static class Security
    {
        public static bool CheckAuth(string server)
        {
            var authResponse = Auth(server);
            if (authResponse == null || authResponse.IsError)
            {
                var error = "Авторизация не пройдена. Обратитесь к администратору.";
                Logger.Main.Error(error);

                return false;
            }

            return true;
        }

        private static DTO.Response.AuthResponse Auth(string server)
        {
            try
            {
                var serviceClient = ServiceLocator.Current.GetInstance<ServiceClient>();

                return serviceClient.Auth(Cache.Instance.GetIdentity().UserId, Properties.Settings.Default.ApiServer);
            }
            catch (Exception exception)
            {
                Logger.Main.ErrorException("AuthViewModel.RedirectCommandExecute", exception);

                return null;
            }
        }
    }
}
