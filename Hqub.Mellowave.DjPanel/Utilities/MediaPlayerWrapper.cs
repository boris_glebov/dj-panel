﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;
using Hqub.Mellowave.DjPanel.DTO;

namespace Hqub.Mellowave.DjPanel.Utilities
{
    public class MediaPlayerWrapper
    {
        private MediaPlayer _mediaPlayer;
        private DispatcherTimer _timer;
        private bool _isBlockAutoChangePosition;

        private Track _currentPlays;

        /// <summary>
        /// Возвращает битрейт трэка на удаленном сервере
        /// </summary>
        /// <param name="url"></param>
        /// <param name="duration"></param>
        /// <returns></returns>
        public static int GetBitrate(string url, int duration)
        {
            try
            {
                var length = WebBrowserUtility.GetRemoteFileSize(url);

                if (length == -1)
                    return 0;

                return length*8/duration/1000;
            }
            catch (Exception exception)
            {
                Logger.Main.ErrorException(string.Format("GetBitrate error. {0}", exception.Message), exception);
                return 0;
            }
        }

        //В это поле сохраняется значение длительности трэка, так как 
        //для отображения прогресса меняем значения поля самого объекта:
        private int _currentPlaysTrackDuration;

        public MediaPlayerWrapper()
        {
            _mediaPlayer = new MediaPlayer();
           
            _mediaPlayer.MediaEnded += (mediaSender, mediaArgs) =>
                                           {
                                               var track = _currentPlays;

                                               Stop(false);

                                               OnMediaEnd(track);
                                           };
            
            // Настраиваем таймер:
            _timer = new DispatcherTimer(DispatcherPriority.Background) {Interval = TimeSpan.FromSeconds(1)};

            _timer.Tick += (timerSender, timerArgs) =>
                               {
                                   _currentPlays.Duration = _currentPlaysTrackDuration -
                                                            (int) _mediaPlayer.Position.TotalSeconds;


                                   _currentPlays.Position = (int) _mediaPlayer.Position.TotalSeconds;

                                   if(_currentPlays.SliderVisibility != 1)
                                   {
                                       _currentPlays.SliderPosition = (int) _mediaPlayer.Position.TotalSeconds;
                                   }
                               };
        }

        #region Events

        public delegate void MediaEndHandler(Track track);

        public event MediaEndHandler MediaEnd;

        protected virtual void OnMediaEnd(Track track)
        {
            var handler = MediaEnd;
            if (handler != null) handler(track);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Запустить проигрывание трэка
        /// </summary>
        /// <param name="track">Ссылка на трэк</param>
        public void Play(Track track)
        {
            var trackUrl = track.VkUrl;

            System.Diagnostics.Debug.WriteLine("Play: {0}", trackUrl);

            //Останавливаем текущий играющий трэк:
            if (_currentPlays != null)
            {
                //Если трэк был поставлен на паузу, то просто 
                //продолжаем его проигрывание.
                if (track == _currentPlays && !track.IsPlays)
                {
                    _currentPlays.IsPlays = true;
                    _currentPlays.ProgressBarVisibility = Visibility.Visible;
                    _mediaPlayer.Play();
                    _timer.Start();

                    return;
                }

                if (track == _currentPlays && track.IsPlays)
                {
                    Stop();
                    return;
                }

                Stop();

                _currentPlays.Duration = _currentPlaysTrackDuration;
            }

            _currentPlays = track;

            //Запоминаем искомую длительность:
            _currentPlaysTrackDuration = track.Duration;
            track.TotalDuration = track.Duration;

            _currentPlays.IsPlays = true;
            _currentPlays.ProgressBarVisibility = Visibility.Visible;

            _mediaPlayer.Open(new Uri(trackUrl));

            _mediaPlayer.Play();
            _timer.Start();
        }

        /// <summary>
        /// Остановить проигрывание трэка
        /// </summary>
        /// <param name="pause">По умолчанию true. Ставит трэк на паузу.</param>
        public void Stop(bool pause = true)
        {
            if (_currentPlays == null || !_currentPlays.IsPlays) return;

            _currentPlays.IsPlays = false;
            _currentPlays.ProgressBarVisibility = Visibility.Hidden;
            
            _timer.Stop();

            if (!pause)
            {
                _mediaPlayer.Stop();
                _currentPlays.Duration = _currentPlaysTrackDuration;
                _currentPlays = null;
            }

            _mediaPlayer.Pause();
        }

        /// <summary>
        /// Перемотка до требуемой позиции.
        /// </summary>
        /// <param name="position">Позиция в секундах.</param>
        public void Rewind(double position)
        {
            if (_currentPlays == null)
                return;

            System.Diagnostics.Debug.WriteLine(_currentPlays.SliderVisibility);

            if (_currentPlays.SliderVisibility == 1)
                _mediaPlayer.Position = TimeSpan.FromSeconds(position);
        }

        /// <summary>
        /// Устанавливаем\Снимает блокировку на изменении позиции прогресбара плеера в таймере.
        /// </summary>
        public void SetBlockAutoChangePosition(bool value)
        {
            _isBlockAutoChangePosition = value;
        }

        /// <summary>
        /// Статус трэка
        /// </summary>
        public bool IsPlayed(Track track)
        {
            return _currentPlays == track && _currentPlays.IsPlays;
        }

        /// <summary>
        /// Этот трэк сейчас играет?
        /// </summary>
        /// <param name="track"></param>
        public bool IsCurrent(Track track)
        {
            return _currentPlays == track;
        }

        #endregion
    }
}
