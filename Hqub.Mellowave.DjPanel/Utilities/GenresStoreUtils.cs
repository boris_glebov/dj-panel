﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hqub.Mellowave.DjPanel.DTO;
using Microsoft.Practices.ServiceLocation;

namespace Hqub.Mellowave.DjPanel.Utilities
{
    public class GenresStoreUtils
    {
        private static GenresStoreUtils _genresStore;

        private GenresStoreUtils()
        {
            Genres = new List<Genre>(LoadGenres());
        }

        public static GenresStoreUtils GenresStore
        {
            get
            {
                return _genresStore = _genresStore ?? new GenresStoreUtils();
            }
        }

        private IEnumerable<Genre> LoadGenres()
        {
            var service = ServiceLocator.Current.GetInstance<ServiceClient>();

            return service.GetGenres();
        }

        public List<Genre> Genres { get; set; }
    }
}
