﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hqub.Mellowave.DjPanel
{
    public static class MetaInfo
    {
        public const int Major = 0;
        public const int Minor = 9;

        public const string AssemblyVersion = "0.9.3";
    }
}
