﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Hqub.FileManager.Controllers
{
    public class DownloaderController : Controller
    {
        //
        // GET: /Downloader/

        public ActionResult Get(string fileName)
        {
            var path = string.Format("{0}{1}.mp3", Properties.Settings.Default.MusicFolder, fileName);

            if (!System.IO.File.Exists(path))
                return new HttpNotFoundResult(string.Format("404. File '{0}' not found.", fileName));

            return File(System.IO.File.ReadAllBytes(path), "audio/mpeg");
        }
    }
}
