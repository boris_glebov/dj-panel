﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.ServiceModel.Activation;
using Hqub.Mellowave.DjPanel.Kernel.Store;
using IronMQ;
using MongoRepository;
using System.Linq;

namespace Hqub.Mellowave.DjPanel.Kernel
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public partial class Kernel : IKernel
    {
        /// <summary>
        /// Сохраняем станцию в БД
        /// </summary>
        public bool SaveRadioStation(RadioStation station)
        {
            try
            {
                if (SaveRadioStationToMongoDB(station))
                {
                    //Отправляем станцию в очередь на обработку.
                    return SendNotificationToMQ(station);
                }

                return false;
            }
            catch (Exception exception)
            {
                Logger.Main.ErrorException("SaveRadioStation", exception);

                return false;
            }
        }


        /// <summary>
        /// Возвращает список радио станций
        /// </summary>
        /// <param name="groupName">атрибут отбора по группам</param>
        public IEnumerable<RadioStation> GetStations(string groupName)
        {
            var radioStationRepository = new MongoRepository<RadioStation>();

            if (string.IsNullOrEmpty(groupName) || groupName.ToLower() == "all")
            {
                return radioStationRepository.All();
            }

            var stations = radioStationRepository.All(x => x.Group == groupName);

            return stations;
        }


        /// <summary>
        /// Вовзращает все трэки радио станции из плейлиста по умолчанию.
        /// В дальнейшем метод будет рассширен атрибут уточнения плейлиста.
        /// </summary>
        /// <param name="stationId">Идентфиикатор станции</param>
        public IEnumerable<Track> GetTracks(string stationId)
        {
            var radioStationRepository = new MongoRepository<RadioStation>();
            var tracksRepository = new MongoRepository<Tracks>();

            var station = radioStationRepository.GetById(stationId);

            var defaultPlaylist = station.Playlists.FirstOrDefault();
            if(defaultPlaylist == null)
                return new List<Track>();

            return defaultPlaylist.Tracks;
        }

        /// <summary>
        /// Проверяем загружен трэк в БД или нет
        /// </summary>
        public bool ExistsTrack(string trackInfoId)
        {
            var repository = new MongoRepository.MongoRepository<Tracks>();

            var track = repository.GetById(trackInfoId);
            return track != null && !string.IsNullOrEmpty(track.Hash);
        }


        /// <summary>
        ///  Сохраняем аудио файл из ВК (или другого източника) в БД
        /// </summary>
        /// <param name="trackId">Id трэка</param>
        /// <param name="data">Файл</param>
        public void SaveAudioFile(string trackId, byte[] data)
        {
            try
            {
                // Сохраняем файл в БД
                using (
                    var fileStream =
                        File.Create(string.Format("{0}{1}.mp3", Properties.Settings.Default.MusicFolder, trackId),
                                    data.Length, FileOptions.None))
                {
                    var formatter = new BinaryFormatter();

                    formatter.Serialize(fileStream, data);
                }

                // Кладем уведомление в очередь
//                var fileTransfer = new Store.IronMQ.FileTransfer {TrackId = trackId};
//
//                SendToMQ(Properties.Settings.Default.FileTransferMQName, fileTransfer.ToString());
            }
            catch (Exception exception)
            {
                Logger.Main.FatalException(string.Format("SaveAudioFile. {0}", exception.Message), exception);
            }
        }


        #region Methods

        /// <summary>
        /// Сохраняем станцию в БД
        /// </summary>
        /// <param name="station"></param>
        /// <returns></returns>
        private bool SaveRadioStationToMongoDB(RadioStation station)
        {
            var radioStationRepository = new MongoRepository<RadioStation>();

            // Обновим TrackInfo
            foreach (var track in station.Playlists.Select(playlist => playlist.Tracks.Where(x => string.IsNullOrEmpty(x.TrackInfo))).SelectMany(tracks => tracks))
            {
                track.TrackInfo = MongoDB.Bson.ObjectId.GenerateNewId().ToString();
            }

            radioStationRepository.Update(station);

            return true;
        }

        /// <summary>
        /// Отправляем уведомление об изменении радио станции в очередь сообщений
        /// </summary>
        /// <param name="station">Объект измененной радио станции</param>
        private bool SendNotificationToMQ(RadioStation station)
        {
            var container = new Store.IronMQ.RadioStationUpdateCommandContainer(station.Id, station.Name);

            SendToMQ(Properties.Settings.Default.IronMQueueName, container.ToString());

            return true;
        }

        /// <summary>
        /// Служебный метод для отправки сообщений в очередь.
        /// </summary>
        /// <param name="nameMQ">Имя очереди</param>
        /// <param name="message">Сообщение в формате JSON</param>
        private void SendToMQ(string nameMQ, string message)
        {
            var client = new Client(Properties.Settings.Default.IronMQProjectId, Properties.Settings.Default.IronMQToken);

            var queue = new Queue(client, nameMQ);
            queue.Push(message);
        }

        #endregion
    }
}
