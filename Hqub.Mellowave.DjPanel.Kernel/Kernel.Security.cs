﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Hqub.Mellowave.DjPanel.Kernel.Store;
using MongoRepository;

namespace Hqub.Mellowave.DjPanel.Kernel
{
    public partial class Kernel
    {
        public bool CheckIdentity(string clientId)
        {
            var userRepositoryManager = new MongoRepository<User>();

            return userRepositoryManager.Exists(x => x.ClientId == clientId);
        }
    }
}