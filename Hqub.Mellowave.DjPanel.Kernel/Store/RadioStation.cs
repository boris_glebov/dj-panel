﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Web;
using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization.Attributes;

namespace Hqub.Mellowave.DjPanel.Kernel.Store
{
    [DataContract]
    public class RadioStation : MongoRepository.Entity
    {
        [DataMember]
        [BsonIgnore]
        public string EntityId { get { return Id; } set { Id = value; } }

        [DataMember]
        [BsonElement("name")]
        public string Name { get; set; }

        [DataMember]
        [BsonElement("group")]
        public string Group { get; set; }

        [DataMember]
        [BsonElement("notes")]
        public string Notes { get; set; }

        [DataMember]
        [BsonElement("url")]
        public string Url { get; set; }

        [DataMember]
        [BsonElement("playlists")]
        public List<Playlist> Playlists { get; set; }

        [DataMember]
        [BsonElement("station_id")]
        public string StationId { get; set; }

        [DataMember]
        [BsonElement("backlog")]
        public List<string> Backlog { get; set; }


            #region Служебные св-ва

        [DataMember]
        [BsonIgnore]
        public bool IsPlays { get; set; }

        #endregion

        #region Methods

        public override string ToString()
        {
            var jsonText = new StringBuilder();

            using(var textWriter = new System.IO.StringWriter(jsonText))
            using(var jsonWriter = new Newtonsoft.Json.JsonTextWriter(textWriter))
            {
                var serializer = new Newtonsoft.Json.JsonSerializer();
                serializer.Serialize(jsonWriter, this);

                System.Diagnostics.Debug.WriteLine(jsonText);
            }

            return jsonText.ToString();
        }

        #endregion

    }
}