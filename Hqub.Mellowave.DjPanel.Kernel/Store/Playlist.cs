﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Hqub.Mellowave.DjPanel.Kernel.Store
{
    [DataContract]
    public class Playlist : MongoRepository.Entity
    {
        [DataMember]
        [MongoDB.Bson.Serialization.Attributes.BsonElement("name")]
        public string Name { get; set; }

        [DataMember]
        [MongoDB.Bson.Serialization.Attributes.BsonElement("tracks")]
        public List<Track> Tracks { get; set; }

        [DataMember]
        [MongoDB.Bson.Serialization.Attributes.BsonElement("path")]
        public string Path { get; set; }
    }
}