﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using MongoDB.Bson.Serialization.Attributes;
using MongoRepository;

namespace Hqub.Mellowave.DjPanel.Kernel.Store
{
    public class AudioFiles : Entity
    {
        public AudioFiles()
        {
            
        }

        [DataMember]
        [BsonElement("data")]
        public string Data { get; set; }

    }
}