﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Hqub.Mellowave.DjPanel.Kernel.Store
{
    [DataContract]
    public class Track : MongoRepository.Entity
    {
        #region .ctor

        public Track()
        {

        }

        // Создает экземпляр класса Track на основе данных из ВК.
        public Track(Vkontakte.API.Model.Audio.Audio audio)
        {
            VkId = audio.Id;
            Title = audio.Title;
            Artist = audio.Artist;
            VkUrl = audio.Url;
            Duration = audio.Duration;
        }

        #endregion

        [DataMember]
        [BsonIgnore]
        public string EntityId
        {
            get { return Id; }
            set { Id = value; }
        }

        /// <summary>
        /// Vkontakte ID
        /// </summary>
        [DataMember]
        [BsonElement("vkid")]
        public string VkId { get; set; }

        /// <summary>
        /// MusicBrainz ID
        /// </summary>
        [DataMember]
        [BsonElement("mbid")]
        public string MBId { get; set; }

        /// <summary>
        /// Hash файла
        /// </summary>
        [DataMember]
        [BsonElement("hash")]
        public string Hash { get; set; }

        [DataMember]
        [BsonElement("title")]
        public string Title { get; set; }

        [DataMember]
        [BsonElement("artist")]
        public string Artist { get; set; }

        [DataMember]
        [BsonElement("vk_url")]
        public string VkUrl { get; set; }

        [DataMember]
        [BsonRepresentation(BsonType.ObjectId)]
        [BsonElement("track_info")]
        public string TrackInfo { get; set; }

        [DataMember]
        [BsonElement("duration")]
        public int Duration { get; set; }

        [DataMember]
        [BsonElement("file_path")]
        public string FilePath { get; set; }

        #region Служебные поля

        [DataMember]
        [BsonIgnore]
        public bool IsPlays { get; set; }

        [DataMember]
        [BsonIgnore]
        public string FullName
        {
            get { return string.Format("{0} - {1}", Artist, Title); }
            set { }
        }

        [DataMember]
        [BsonIgnore]
        public bool IsDownloaded
        {
            get
            {
                return false;

//                if(string.IsNullOrEmpty(TrackInfo))
//                {
//                    return false;
//                }
//
//#if DEBUG
//                return true;
//#endif
//
//                var repository = new MongoRepository.MongoRepository<Tracks>();
//
//                return repository.Exists(x => x.Id == TrackInfo && !string.IsNullOrEmpty(x.Hash));
            }
            set { }
        }

        #endregion
    }
}