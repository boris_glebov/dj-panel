﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using MongoRepository;

namespace Hqub.Mellowave.DjPanel.Kernel.Store
{
    [KnownType(typeof(RadioStation))]
    [DataContract]
    public class RadioStationGroup : IEntity
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public RadioStation Stations { get; set; }

        [DataMember]
        public string Id { get; set; }
    }
}