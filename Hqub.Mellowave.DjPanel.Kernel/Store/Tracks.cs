﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson.Serialization.Attributes;

namespace Hqub.Mellowave.DjPanel.Kernel.Store
{
    public class Tracks : MongoRepository.Entity
    {
        [BsonElement("hash")]
        public string Hash { get; set; }
    }
}