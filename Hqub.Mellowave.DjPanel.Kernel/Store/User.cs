﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Hqub.Mellowave.DjPanel.Kernel.Store
{
    [DataContract]
    public class User : MongoRepository.Entity
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Surname { get; set; }

        [DataMember]
        public string ClientId { get; set; }
    }
}