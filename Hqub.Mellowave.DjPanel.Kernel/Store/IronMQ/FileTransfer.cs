﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hqub.Mellowave.DjPanel.Kernel.Store.IronMQ
{
    public class FileTransfer : Container
    {
        public FileTransfer()
        {
            Operation = "UploadAudioFile";
        }

        public string TrackId { get; set; }
    }
}