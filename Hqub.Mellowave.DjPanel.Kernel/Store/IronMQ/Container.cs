﻿using System.Text;

namespace Hqub.Mellowave.DjPanel.Kernel.Store.IronMQ
{
    public class Container
    {
        public string Operation { get; set; }

        public override string ToString()
        {
            var jsonText = new StringBuilder();

            using (var textWriter = new System.IO.StringWriter(jsonText))
            using (var jsonWriter = new Newtonsoft.Json.JsonTextWriter(textWriter))
            {
                var serializer = new Newtonsoft.Json.JsonSerializer();
                serializer.Serialize(jsonWriter, this);
            }

            return jsonText.ToString();
        }
    }
}