﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Newtonsoft.Json.Bson;

namespace Hqub.Mellowave.DjPanel.Kernel.Store.IronMQ
{
    public class RadioStationContainer : Container
    {
        public RadioStationContainer(RadioStation station)
        {
            Operation = "SaveRadioStation";

            Data = station;
        }

        public string Operation { get; set; }

        public RadioStation Data { get; set; }
    }
}