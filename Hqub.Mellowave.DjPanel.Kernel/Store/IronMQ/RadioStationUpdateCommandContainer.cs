﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hqub.Mellowave.DjPanel.Kernel.Store.IronMQ
{
    public class RadioStationUpdateCommandContainer : Container
    {
        public RadioStationUpdateCommandContainer(string Id, string stationName)
        {
            Operation = "RadioStationChanged";

            StationId = Id;
            StationName = stationName;
        }

        public string StationId { get; set; }

        public string StationName { get; set; }
    }
}