﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hqub.Mellowave.DjPanel.Kernel
{
    public class Constants
    {
        public const string IronMQ = "IronMQ";
        public const string MongoDB = "MongoDB";
    }
}