﻿using System.Collections.Generic;
using System.ServiceModel;
using Hqub.Mellowave.DjPanel.Kernel.Store;

namespace Hqub.Mellowave.DjPanel.Kernel
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IKernel" in both code and config file together.
    [ServiceContract]
    public interface IKernel
    {
        #region Security

        [OperationContract]
        bool CheckIdentity(string clientId);

        #endregion

        #region Kernel

        [OperationContract]
        IEnumerable<RadioStation> GetStations(string groupName);

        [OperationContract]
        bool SaveRadioStation(RadioStation station);

        [OperationContract]
        IEnumerable<Track> GetTracks(string stationId);

        [OperationContract]
        void SaveAudioFile(string trackId, byte[] data);

        [OperationContract]
        bool ExistsTrack(string trackInfoId);

        #endregion
    }
}
