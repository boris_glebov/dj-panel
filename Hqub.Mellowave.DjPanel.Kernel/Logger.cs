﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hqub.Mellowave.DjPanel.Kernel
{
    public static class Logger
    {
        public static NLog.Logger Main
        {
            get { return NLog.LogManager.GetLogger("Main"); }
        }
    }
}