﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hqub.Mellowave.DjPanel.Console.Test
{
    class Program
    {
        static void Main(string[] args)
        {
//            var api = new Vkontakte.API.API();
//            
//            System.Console.WriteLine( api.SearchAudio());

            CheckService();

            System.Console.ReadKey();
        }

        #region Test Kernel

        private static void CheckService()
        {
            using(var client = new ServiceReference1.KernelClient())
            {
                GetStationsTest(client);
            }
        }

        private static void GetStationsTest(ServiceReference1.KernelClient client)
        {
            foreach (var radioStation in client.GetStations("All"))
            {
                System.Console.WriteLine("{0}. {1}",radioStation.EntityId, radioStation.Name);
            }
        }

        #endregion
    }
}
